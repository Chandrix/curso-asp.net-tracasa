import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PropiedadesInputComponent } from './propiedades-input.component';

describe('PropiedadesInputComponent', () => {
  let component: PropiedadesInputComponent;
  let fixture: ComponentFixture<PropiedadesInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PropiedadesInputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PropiedadesInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
