﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRUD_MVC_EF_ASPCore.Models
{
    public class TeacherContext : DbContext
    {
        public TeacherContext(DbContextOptions<TeacherContext> options) : base(options) { }
        //Corresponde a una estructura tipo Set
        //(otro tipo de coleccion como List o Dicc) cuya persistencia sera en base de datos
        public DbSet<Teacher> Teachers { get; set; }
        //metodo evento que va a ser llamado al configurarse la base de datos

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                Console.WriteLine("La bbdd no ha sido configurada");
            }
            else
            {
                Console.WriteLine("La bbdd Sí ha sido congiurada");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Teacher>(
                entity =>
                {
                    entity.Property(e => e.Name)
             .IsRequired().HasMaxLength(50)
             .IsUnicode(false);

                    entity.Property(e => e.Skills)
             .IsRequired().HasMaxLength(50)
             .IsUnicode(false);

                }
                );
        }
    }
}
