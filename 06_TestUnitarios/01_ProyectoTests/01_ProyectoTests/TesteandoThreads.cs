﻿using NUnit.Framework;
using System;
using System.Threading;

namespace _01_ProyectoTests
{
    class TesteandoThreads
    {
        [Test]
        public void ProbandoHilos()
        {
            Console.WriteLine("Arrancando ProbandoHilos");
            Thread hilo1 = new Thread(FuncionHilo1);
            Thread hilo2 = new Thread(FuncionHilo1);
            hilo1.Start();
            hilo2.Start();
            Thread.Sleep(100);
            Console.WriteLine("Terminando ProbandoHilos");
        }
        public static void FuncionHilo1()
        {
            Console.WriteLine("Arrancando hilo1");
            for (int i = 0; i < 1000000000; i++)
            {
                Console.WriteLine("Terminando hilo1");
            }
        }
        public static void FuncionHilo2()
        {
            Console.WriteLine("Arrancando hilo2");
            for (int i = 1000000000; i < 0; i++)
            {
                Console.WriteLine("Terminando hilo2");
            }
        }
    }
}
