﻿using NUnit.Framework;
using System;

namespace _01_ProyectoTests
{
    public class Tests
    {
        string texto;
        // Este es el método de inicialización porque lleva el atributo C# [SetUp]
        // Los atributos son "decoradores", características que añadimos a clases, propiedades y métodos para agregar cierta funcionalidad.
        [SetUp]
        public void Inicializacion()
        {
            texto = "Texto inicial";
        }

        [Test]
        public void TestPrimero()
        {
            // Assert = asegurar.
            Assert.Pass();
        }
        [Test]
        public void TestSegundo()
        {
            Assert.AreEqual(1 + 3, 2 + 2);
            Assert.AreNotEqual(1 + 3, 2 + 3);
            Assert.IsNotNull(this.texto);
        }
        [Test]
        public void TestTercero()
        {
            texto = "";
            Assert.IsEmpty(this.texto);
        }
        public static void DelegadoCualquieraOk()
        {
            Console.WriteLine("Delegado Cualquiera Ok");
        }
        public static void DelegadoCualquieraMal()
        {
            throw new NotImplementedException("Delegado Cualquiera Mal");
        }
        [Test]
        public void TestCuarto()
        {
            Console.WriteLine("Antes del test 4");
            Assert.IsTrue(texto.Equals("Texto inicial"), "El texto no se ha inicializado");
            Assert.Contains('i', texto.ToCharArray(), "El array de char de texto no tiene 'i'");
            TestDelegate delegateOK = DelegadoCualquieraOk;
            Assert.DoesNotThrow(delegateOK, "Delegado Cualquiera Ok Falló");
            Assert.Throws<NotImplementedException>(DelegadoCualquieraMal, "Delegado Cualquiera Ok cascó");
            Console.WriteLine("Despues del test 4");
        }
        [Test(Author = "Andrés", Description = "Test para probar throws")]
        public void TestProbandoThrowsSimple()
        {
            // Tipo exacto
            TestDelegate delegadomal = DelegadoCualquieraMal;
            Assert.Throws(typeof(NotImplementedException), delegadomal, "No quiere ir");

            // Tipo derivado
            Assert.Throws(Is.InstanceOf(typeof(Exception)), delegadomal, "Tampoco va");

            // Lambda
            Assert.Throws(typeof(NotImplementedException),
            () => { throw new NotImplementedException("Lambda mal"); }, "Sigue sin ir");

            // Delegado anónimo
            Assert.Throws(typeof(NotImplementedException),
            delegate { throw new NotImplementedException("Delegado anonimo mal"); }, "Pues eso");
        }
    }
}
