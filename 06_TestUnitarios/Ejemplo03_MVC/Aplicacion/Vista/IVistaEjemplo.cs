﻿namespace Ejemplo03_MVC.Aplicacion.Vista
{
    interface IVistaEjemplo
    {
        // ¿Es necesario?    Pues en principio NO
        /*ControladorEjemplo Controlador
        {
            get; set;
        }*/
        public void Menu();
        public void AltaEjemplo();
        public void MostrarEjemplos();
    }
}
