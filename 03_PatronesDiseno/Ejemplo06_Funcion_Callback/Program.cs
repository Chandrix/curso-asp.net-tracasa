﻿using System;

namespace Ejemplo06_Funcion_Callback
{
    // Creamos un nuevo tipo de dato, que indica que es una función (ni clase, ni interfaz...)
    // donde lo que importa son los tipos de datos que recibe y que devuelve.
    // Es decir, declaramos un delegado.
    delegate float FuncionOperador(float op1, float op2);
    // Ahora podremos crear variables de tipo float función (float, float).
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Invocamos a la suma");
            // Al pasar como parametro la función SumarA no lleva paréntesis, porque no se llama ahora, si no después.
            VistaCalculadora(Calculadora_A.SumarA);
            VistaCalculadora(Calculadora_B.MultiplicarB);
            // VistaOperadora(Calculadora_B.MultiplicarB, Calculadora_B.SumarB);
        }
        // Esta función necesita de una función callback para saber como tiene que calcular.
        static void VistaCalculadora(FuncionOperador operador)
        {
            Console.WriteLine("Operando 1: ");
            float x = float.Parse(Console.ReadLine());
            Console.WriteLine("Operando 2: ");
            float y = float.Parse(Console.ReadLine());
            float resultado = operador(x, y);
            Console.WriteLine("Resultado: " + Math.Round(resultado, 2));
        }

        /* static void VistaOperadora(FuncionOperador funSuma, FuncionOperador funMultiplicar)
         {
             Console.WriteLine("Introduzca la operacion a resolver: ");
             string pedir = Console.ReadLine();
             int signo = pedir.IndexOf("+");
             if (signo < 0)
             {
                 signo = pedir.IndexOf("*");
                 if (signo < 0)
                 {
                     Console.WriteLine("No te entiendo, sucio humano");
                     return;
                 }
                 float x = float.Parse(pedir.Substring(0, signo));
                 float y = float.Parse(pedir.Substring(0, signo));
                 Console.WriteLine("Resultado: " + );
             }
         }*/
        // Ejercicio1: Usar VistaCalculadora con una calc creada por vosotros pero exacta. Llamad a la clase CalculadoraB.
        // Ejercicio2: Crear una Vista operadora (fun estática) en la que el usuario pueda introducir una única cadena, con dos números y el operador,
        // que calcule usando CalcA o CalcB u otra diferente. Si lo pone mal que diga "No te entiendo humano"
        // Ejemplos: 10+22, 333*1 o 2.5*11. 
    }
}
