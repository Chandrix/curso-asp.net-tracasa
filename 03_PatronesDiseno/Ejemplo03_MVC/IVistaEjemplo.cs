﻿namespace Ejemplo03_MVC
{
    interface IVistaEjemplo
    {
        public void AltaEjemplo();
        public void MostrarEjemplos();
        public void Menu();
    }
}