﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC
{
    class VistaEjemplo : IVistaEjemplo
    {
        ControladorEjemplo controlador;

        public VistaEjemplo(ControladorEjemplo controlador)
        {
            this.controlador = controlador;
            // Guardamos y usamos el controlador
        }
        public void Menu()
        {
            int opcion = 0;
            do
            {
                Console.WriteLine("MENU: ( 0 -Salir)");
                Console.WriteLine(" 1 - Alta ejemplo");
                Console.WriteLine(" 2 - Mostrar todos");
                Console.WriteLine(" 3 - Mostrar uno");
                string str_op = Console.ReadLine();
                if (int.TryParse(str_op, out opcion))
                {
                    switch (opcion)
                    {
                        case 1:
                            AltaEjemplo();
                            break;
                        case 2:
                            MostrarEjemplos();
                            break;
                        case 3:
                            MostrarUno();
                            break;
                        default:
                            Console.WriteLine("Introduzca opcion valida");
                            break;
                    }
                }
                else
                {
                    opcion = -1;
                    Console.WriteLine("Escribe bien el número");
                }
            } while (opcion != 0);
        }
        public void AltaEjemplo()
        {
            Console.WriteLine("Alta ejemplo: numero: ");
            int entero = int.Parse(Console.ReadLine());
            Console.WriteLine("Alta ejemplo: string: ");
            string str = Console.ReadLine();
            this.controlador.AltaEjemplo(entero, str);
            // TODO: En vez de usar modelo usamos controlador.
        }
        public void MostrarEjemplos()
        {
            // TODO: En vez de usar modelo usamos controlador.
            IEnumerable<Ejemplo> todos = controlador.LeerEjemplos();
            foreach (Ejemplo ejemplo in todos)
            {
                Console.WriteLine("Ejemplo " + ejemplo.ToString());
            }
        }
        public void MostrarUno()
        {
            Console.WriteLine(" 1 - Busqueda por numero ");
            Console.WriteLine(" 2 - Busqueda por nombre ");
            int opcion;
            string op = Console.ReadLine();
            if (int.TryParse(op, out opcion))
            {
                switch (opcion)
                {
                    case 1:
                        {
                            Console.WriteLine("Introduce el numero: ");
                            int numero = int.Parse(Console.ReadLine());
                            Ejemplo ejemplo = controlador.LeerUno(numero);
                            if (ejemplo != null)
                                Console.WriteLine("Ejemplo " + ejemplo.ToString());
                            else
                                Console.WriteLine("No encontrado por " + numero);
                            break;
                        }
                    case 2:
                        {
                            Console.WriteLine("Introduce el nombre: ");
                            string nombre = Console.ReadLine();
                            Ejemplo ejemplo = controlador.LeerUno(nombre);
                            if (ejemplo != null)
                                Console.WriteLine("Ejemplo " + ejemplo.ToString());
                            else
                                Console.WriteLine("No encontrado por " + nombre);
                            break;
                        }
                }
            }
        }
        public void ModificarUno()
        {
            Console.WriteLine("Intro nombre a buscar: ");
            string nombre = Console.ReadLine();
            if (controlador.LeerUno(nombre) == null)
            {
                Console.WriteLine("No se ha encontrado " + nombre);
            }
            else
            {
                Console.WriteLine("Nuevo numero:");
                int entero = int.Parse(Console.ReadLine());
                Console.WriteLine("Nuevo string:");
                string str = Console.ReadLine();
                Ejemplo ej = this.controlador.Modificar(nombre, entero, str);

                if (ej != null)
                    Console.WriteLine("Modificado " + ej.ToString());
                else
                    Console.WriteLine("No encontrado por " + nombre);
            }
        }
    
    }
}

