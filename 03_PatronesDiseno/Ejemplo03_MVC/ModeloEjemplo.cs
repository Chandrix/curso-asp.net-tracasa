﻿using System;
using System.Collections.Generic;
using System.Text;
namespace Ejemplo03_MVC
{
    class ModeloEjemplo : IModeloEjemplo, IModeloGenerico<Ejemplo>
    {
        private List<Ejemplo> ejemplos;

        public ModeloEjemplo()
        {
            ejemplos = new List<Ejemplo>();
        }

         IList<Ejemplo> IModeloGenerico<Ejemplo>.LeerTodos()
        {
            return LeerTodos();
        }
        public Ejemplo Crear(Ejemplo ejemplo)
        {
            ejemplos.Add(ejemplo);
            return ejemplo;
        }
        public bool Eliminar(string nombre)
        {
            Ejemplo ejemplo = LeerUno(nombre);
                if(ejemplo != null)
            {
                return ejemplos.Remove(ejemplo);
            }
            return false;
        }
        public bool Eliminar(int entero)
        {
            Ejemplo ejemplo = LeerUno(entero);
            if (ejemplo != null)
            {
                return ejemplos.Remove(ejemplo);
            }
            return false;
        }
        public List<Ejemplo> LeerTodos()
        {
            return ejemplos;
        }
        public Ejemplo LeerUno(string str)
        {
            foreach (Ejemplo ejemplo in ejemplos)
            {
                if (ejemplo.Str == str)
                    return ejemplo;
            }
            return null;
        }
        public Ejemplo LeerUno(int entero)
        {
            foreach (Ejemplo ejemplo in ejemplos)
            {
                if (ejemplo.EsEntero(entero))
                    return ejemplo;
            }
            return null;
        }

        public Ejemplo Crear(int entero, string str)
        {
            return Crear(new Ejemplo(entero, str));
        }

        IList<Ejemplo> IModeloEjemplo.LeerTodos(string nombre)
        {
            return LeerTodos();
        }
        
        public Ejemplo Modificar(string nombreBusq, int entero, string str)
        {
            Ejemplo ejemploamodif = LeerUno(nombreBusq);
            if (ejemploamodif == null)
                return null;
            int posicion = ejemplos.IndexOf(ejemploamodif);
            ejemplos[posicion] = new Ejemplo(entero, str);
            return ejemplos[posicion];
        }
        // De esta manera creamos al final de la lista, alterando el orden.
        public Ejemplo Modificar2(string nombreBusq, int entero, string str)
        {
            Eliminar(nombreBusq);
            return Crear(entero, str);
        }

        IList<Ejemplo> IModeloEjemplo.LeerTodos()
        {
            return LeerTodos();
        }
    }
}
