﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC
{
    class ModeloEjemploDiccionario : IModeloGenerico<Ejemplo>
    {
        Dictionary<string, Ejemplo> diccionario;

        public ModeloEjemploDiccionario()
        {
            diccionario = new Dictionary<string, Ejemplo>();
        }
        public Ejemplo Crear(string key, int entero, string str)
        {
            Ejemplo ej = new Ejemplo(entero, str);
            if (diccionario.ContainsKey(key))
            {
                ej = null;
            }
            else
            {
                diccionario.Add(key, ej);
            }
            return ej;
        }

        public Ejemplo Crear(Ejemplo ejemplo)
        {
            throw new NotImplementedException("No se puede por la clave");
            // TODO: Leer propiedad privada mediante reflection.
        }

        public Ejemplo Crear(int entero, string str)
        {
             Ejemplo ej = new Ejemplo(entero, str);
            string clave = str + "-" + entero;
            if (diccionario.ContainsKey(clave))
            {
                ej = null;
                //diccionario[clave] = ej;
            }
            else
            {
                diccionario.Add(clave, ej);
            }
            return ej;
        }

        public bool Eliminar(string nombre)
        {
            foreach (var item in diccionario)
            {
                if (item.Value.Str.Equals(nombre))
                {
                    diccionario.Remove(item.Key);
                    return true;
                }
            }
            return false;
        }

        public bool Eliminar(int entero)
        {
            throw new NotImplementedException();
        }

        public void LeerTodos(out Dictionary<string, Ejemplo> todos)
        {
            todos = diccionario;
        }

        public IList<Ejemplo> LeerTodos()
        {
            List<Ejemplo> lista = new List<Ejemplo>();
            foreach (var item in diccionario)
            {
                lista.Add(item.Value);
            }
            return lista;
        }

        public Ejemplo LeerUno(string nombre)
        {
            foreach (var item in diccionario)
            {
                if (item.Value.Str.Equals(nombre))
                {
                    return item.Value;
                }
            }
            return null;
        }
        public Ejemplo LeerUno(int entero)
        {
            foreach (var item in diccionario)
            {
                if (item.Value.EsEntero(entero))
                {
                    return item.Value;
                }
            }
            return null;
        }

        public Ejemplo Modificar(string nombreBusq, int entero, string str)
        {
            foreach (var item in diccionario)
            {
                if (item.Value.Str.Equals(nombreBusq))
                {
                    Ejemplo nuevoEjemploModificado = new Ejemplo(entero, str);
                    string clave = item.Key;
                    diccionario[clave] = nuevoEjemploModificado;
                    return nuevoEjemploModificado;
                }
            }
            return null;
        }
        /* public void DevolverTodos(Dictionary<string, Ejemplo> todos)
         {
             todos = diccionario;
         } */
    }
}
