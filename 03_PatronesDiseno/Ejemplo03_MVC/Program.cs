﻿using System;

namespace Ejemplo03_MVC
{
    class Program
    {
        IModeloEjemplo model1;
        VistaEjemplo ve;
        Program()
        {
            this.model1 = new ModeloEjemplo();
            ControladorEjemplo controlador = new ControladorEjemplo(model1);
            this.ve = new VistaEjemplo(controlador);
            // TODO: Instanciamos la vista y el controlador y aquí enganchamos el MVC.
        }
        void ProbarDatos()
        {
            ModeloEjemplo model1 = new ModeloEjemplo();
            model1.Crear(1, "Uno");
            model1.Crear(2, "Dos");
            model1.Crear(3, "Tres");
            /* ve.MostrarUno("Tres");
             ve.MostrarUno("Estres"); */
        }
        static void Main(string[] args)
        {
            Program program = new Program();
            program.ProbarDatos();
            program.ProbarDatos();
            program.ve.Menu();
            // TODO: Llamar al menú.
        }
    }
}
