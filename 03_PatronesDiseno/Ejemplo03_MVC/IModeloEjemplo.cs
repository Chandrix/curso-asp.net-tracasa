﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC
{
    interface IModeloEjemplo
    {
        Ejemplo Crear(int entero, string str);
        
        IList<Ejemplo> LeerTodos();
        public Ejemplo LeerUno(string nombre);

        public bool Eliminar(string nombre);

        public Ejemplo Modificar(string nombreBusq, int entero, string str);
         

        public Ejemplo LeerUno(int entero);
        IList<Ejemplo> LeerTodos(string nombre);
    }
}
