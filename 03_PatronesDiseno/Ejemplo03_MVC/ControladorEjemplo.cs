﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC
{
    class ControladorEjemplo
    {

        
        IModeloEjemplo modelo;
        IVistaEjemplo vista;
        //TODO: Campo IVista
        
        public ControladorEjemplo(IModeloEjemplo modelo)
        {
             this.modelo = modelo;
            // TODO: Y recibir vista.
        }
        public Ejemplo AltaEjemplo(int entero, string str)
        {
            // 1.- Recibimos el parametro del ejemplo, probablemente de la vista.
            // 2.- Guardamos el ejemplo en el modelo.
            return modelo.Crear(entero, str);
        }
        
        public IEnumerable<Ejemplo> LeerEjemplos()
        {
            return modelo.LeerTodos();
        }

        public Ejemplo LeerUno(string nombre)
        {
            return modelo.LeerUno(nombre);
        }
        public Ejemplo LeerUno(int entero)
        {
            return modelo.LeerUno(entero);
        }
        public bool EliminarUno(string nombre)
        {
            return modelo.Eliminar(nombre);
        }

        internal Ejemplo Modificar(string nombre, int entero, string str)
        {
            throw new NotImplementedException();
        }
        // TODO: Parecido para mostrar ejemplos.
    }
}
