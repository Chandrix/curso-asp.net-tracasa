﻿using System;

namespace Ejemplo05_Lambdas
{
    // Vamos a crear un nuevo tipo de dato, que en vez de almacenar información almacene una función con determinada estructura o firma.
    // La estructura es como la interfaz de una función pero sin importar el nombre: Los tipos de datos que recibe y el tipo de dato que devuelve.
    // En C eran punteros a funciones. En Java, hasta Java 8, eran interfaces con una sóla función, en JS funciones flecha.
    // En C# son los delegados.
    delegate void FuncionQueRecibeInt(int parametro);
    class Program
    {
        static void Main(string[] args)
        {
            // Lambdas.
            // Invocar funciones estáticas.
            FuncionEstatica(5);
            OtraEstatica(7);
            // Variables de tipo función (delegados)
            FuncionQueRecibeInt funRecint;
            funRecint = FuncionEstatica;
            funRecint(200);
            // funRecint = null; Provocaria una excepción.
            funRecint = OtraEstatica;
            funRecint(100);
            Console.WriteLine("\nOtra libreria, otro módulo, otra función, hace:");
            OtroSistema(OtraEstatica);
            OtroSistema(FuncionEstatica);
            OtroSistema(funRecint);
        }

        static void OtroSistema(FuncionQueRecibeInt funcionexterna)
        {
            // Queremos recibir una función como parametro: funcionexterna por ejemplo, que reciba un int.
            int valor = 3;
            Console.WriteLine("Otro sistema hace sus cosas");
            System.Threading.Thread.Sleep(2000);
            Console.WriteLine("Tardar su tiempo");
            Console.WriteLine("Y llamar a la funcionalidad externa");
            funcionexterna(3);
        }
        static void FuncionEstatica(int x)
        {
            Console.WriteLine("No necesito un objeto para ser llamada: ");
            Console.WriteLine("Param: " + x);
        }
        static void OtraEstatica(int y)
        {
            Console.WriteLine(y + " - Otra Estática ");
        }
    }
}
