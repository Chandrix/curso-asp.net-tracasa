﻿// No podemos usar nada de Ejercicio08_Strategy_Lambdas
// ni de Program ni de main()
using System;

namespace Strategy
{
    delegate void StrategyFunc();
    // Ya no se puede heredar de esta clase, si no sobreescribir las funcionalidades mediante delegados ( variables de función, funciones estáticas y/o lambdas.
    /* abstract */
    class StrategyClass
    {
        protected string nombre;
        public Func<string> GetNombre;
        public StrategyClass() : this("")
        {
        }
        public StrategyClass(string mensaje)
        {
            this.GetNombre = () =>
            {
                return mensaje + " " + GetType() + ".Execute()";
            };
            nombre = mensaje + " " + GetType() + ".Execute()";
            mostrarNombre = () =>
            {
                Console.WriteLine(nombre);
            };
        }

        public void RepetirChar(char caracter, int veces)
        {
            for (int i = 0; i < veces; i++)
            {
                Console.Write(caracter);
            }
            Console.Write("\n");
        }
        public StrategyFunc execute;
        // Este método tiene que ser otro delegado. StrategyFunc es igual a Action.
        Action mostrarNombre;
        string type;

        internal StrategyFunc Execute { get => execute; set => execute = value; }
        public Action MostrarNombre { get => mostrarNombre; set => mostrarNombre = value; }
        public string Type { get => type; set => type = value; }

        new string GetType()
        {
            return Type;
        }
    }
}
