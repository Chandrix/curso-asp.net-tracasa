﻿using Strategy;
using System;

namespace Ejercicio08_Strategy_Lambdas
{
    class Program
    {
        static void Main()
        {
            StrategyClass estrategiaA = ConcreteStrategies.NewConcreteStrategyA("Eeeeeeooh ");
            estrategiaA.Execute();

            StrategyClass estrategiaB = ConcreteStrategies.NewConcreteStrategyB();
            estrategiaB.Execute();

            StrategyClass estrategiaC = ConcreteStrategies.NewConcreteStrategyC("Nombre directo");
            estrategiaC.Execute();
        }
    }
    // Una clase abstracta lo principal es que no puede ser instanciada
    // como las interfaces, puede tener algunos métodos implementados
    // y otros sin implementar (declarando sólo su interfaz). Estos
    // métodos son abstractos.  
    static class ConcreteStrategies
    {
        // Siempre podemos hacer delegates por aqui.
        // Estas funciones tienen que funcionar como antes los contructores
        public static StrategyClass NewConcreteStrategyA(string mensaje)
        {
            StrategyClass strObj = new StrategyClass(mensaje);
            strObj.Type = "ConcreteStrategyA";
            /* Dinamicamente le damos la funcionalidad que antes hacíamos por herencia*/
            string textoLocal = "Texto Local";
            strObj.Execute = () =>
            {
                strObj.RepetirChar('-', 30);
                strObj.MostrarNombre();
                Console.WriteLine("Usando var local en lambda: " + textoLocal);
            };
            // Clousure: encapsular/proteger/guardar/almacenar una variable local
            // para que posteriormente pueda usarse en una función lambda.
            bool boolClausurado = true;
            // Aquí estamos "clausurando" la antigua función mostrarNombre, para usarse dentro de la
            // nueva función lamda, mostrarNombre. 
            Action base_MostrarNombre = strObj.MostrarNombre;
            strObj.MostrarNombre = () =>
            {
                strObj.RepetirChar('_', 30);
                base_MostrarNombre();
                strObj.RepetirChar('_', 30);
                // Simplemente por usarse en una función interna a otro bloque de código la variable persiste en el tiempo.
                Console.WriteLine("Usamos la var clausurada" + boolClausurado);
                boolClausurado = false;
            };
            return strObj;
        } // En teoria al finalizar el bloque todas las variables locales se destruyen.
        public static StrategyClass AddConcreteStrategyB(StrategyClass strObj)
        {
            return strObj;
        }
        public static StrategyClass NewConcreteStrategyB()
        {
            StrategyClass strObj = new StrategyClass("Llamar a");
            strObj.Type = "ConcreteStrategyB";
            /* Dinamicamente le damos la funcionalidad que antes hacíamos por herencia*/
            string textoLocal = "Texto Local";
            strObj.Execute = () =>
            {
                strObj.RepetirChar('\n', 3);
                strObj.MostrarNombre();
                Console.WriteLine("Usando var local en lambda: " + textoLocal);
            };
            // Clousure: encapsular/proteger/guardar/almacenar una variable local
            // para que posteriormente pueda usarse en una función lambda.
            bool boolClausurado = true;
            // Aquí estamos "clausurando" la antigua función mostrarNombre, para usarse dentro de la
            // nueva función lamda, mostrarNombre. 
            Action base_MostrarNombre = strObj.MostrarNombre;
            strObj.MostrarNombre = () =>
            {
                base_MostrarNombre();
                strObj.RepetirChar('\n', 3);
                // Simplemente por usarse en una función interna a otro bloque de código la variable persiste en el tiempo.
                Console.WriteLine("Usamos la var clausurada" + boolClausurado);
                boolClausurado = false;
            };
            return strObj;
        }

        public static StrategyClass NewConcreteStrategyC(string nombre)
        {
            StrategyClass strObj = new StrategyClass();
            strObj.Type = "ConcreteStrategyC";
            strObj.GetNombre = () => nombre; // Equivale a { return nombre; } Por usar nombre en una lambda hemos clausurado el parámetro.
            strObj.Execute = () =>
            {
                strObj.RepetirChar('*', 20);
                strObj.MostrarNombre();
                Console.WriteLine(strObj.GetNombre());
            };
            // Clousure: encapsular/proteger/guardar/almacenar una variable local
            // para que posteriormente pueda usarse en una función lambda.
            bool boolClausurado = true;
            // Aquí estamos "clausurando" la antigua función mostrarNombre, para usarse dentro de la
            // nueva función lamda, mostrarNombre. 
            Action base_MostrarNombre = strObj.MostrarNombre;
            strObj.MostrarNombre = () =>
            {
                base_MostrarNombre();
                strObj.RepetirChar('\n', 3);
                // Simplemente por usarse en una función interna a otro bloque de código la variable persiste en el tiempo.
                Console.WriteLine("Usamos la var clausurada" + boolClausurado);
                boolClausurado = false;
            };
            return strObj;
        }
        static void Execute()
        {
            /*RepetirChar();
            MostrarNombre();*/
        }
    }
    // Implementa el algoritmo usando el patron estrategia
    /*class ConcreteStrategyA : StrategyBase
    {
        public ConcreteStrategyA(string mensaje) : base(mensaje)
        {
        }
        public override void Execute()
        {
            RepetirChar('-', 30);
            MostrarNombre();
        }
        public override void MostrarNombre()
        {
            RepetirChar('_', 30);
            base.MostrarNombre();
            RepetirChar('_', 30);
        }
    }

    class ConcreteStrategyB : StrategyBase
    {
        public ConcreteStrategyB() : base("Llamar a")
        {
        }

        public override void Execute()
        {
            MostrarNombre();
            RepetirChar('\n', 3);
        }
    }

    class ConcreteStrategyC : StrategyBase
    {
        public ConcreteStrategyC(string nombre) : base("")
        {
            this.nombre = nombre;
        }
        public override void Execute()
        {
            RepetirChar('*', 20);
            Console.WriteLine("Called ConcreteStrategyC.Execute()");
            Console.WriteLine(this.nombre);
        }
    }*/

    // Contiene un objeto ConcreteStrategy y mantiene una referencia a un objeto Strategy
    /* class Context
    {
        StrategyBase strategy;

        // Constructor
        public Context(StrategyBase strategy)
        {
            this.strategy = strategy;
        }

        public void Execute()
        {
            strategy.Execute();
        }
    }*/
}
