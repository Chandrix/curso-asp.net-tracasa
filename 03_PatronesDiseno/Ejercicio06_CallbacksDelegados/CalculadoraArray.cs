﻿namespace Ejercicio06_CallbacksyDelegados
{
    class CalculadoraArray
    {
        public static float Sumar(float[] nums)
        {
            float sum = 0;
            for (int i = 0; i < nums.Length; i++)
            {
                sum += nums[i];
            }
            return sum;
        }
        public static float Restar(float[] nums)
        {
            float res = nums[0];
            for (int i = 0; i < nums.Length; i++)
            {
                res -= nums[i];
            }
            return res;
        }
        public static float Multiplicar(float[] nums)
        {
            float mult = 0;
            for (int i = 0; i < nums.Length; i++)
            {
                mult *= nums[i];
            }
            return mult;
        }
        public static float Dividir(float[] nums)
        {
            float div = 0;
            for (int i = 0; i < nums.Length; i++)
            {
                div /= nums[i];
            }
            return div;
        }
    }
}
