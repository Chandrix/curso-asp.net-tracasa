﻿using Ejercicio06_CallbacksyDelegados;
using System;
using System.Collections.Generic;
// Ejercicio: Crear dos sistemas (clases independientes que no se conocen entre si):
// El primero trendrá 4 funciones para sumar, restar, multiplicar, dividir, todo con float. Estas podrán realizar la operación sobre un array:
// EJ: {9, 7, 5, 3} suma -> 24  resta -> -6 multiplicación -> 945   {42 / 3 / 2}    división -> (42/ 3) / 2 -> 7    suma -> 47
// {3} -> cualquier operador devuelve 3.
// El array no puede estar vacio.
// La segunda clase le pide al usuario cuantos operandos habrá, y los operandos uno a uno, la operación, y mostrará el resultado.
// Si alguien quiere, en vez de eso, que resuelva: 3+22+11 o 7*3*11*2 
namespace Ejercicio06_CallbacksDelegados
{
    delegate float Funcion(float[] op1);
    class Program
    {
        static void Main(string[] args)
        {
            VistaCalculadora(CalculadoraArray.Sumar, CalculadoraArray.Restar, CalculadoraArray.Multiplicar, CalculadoraArray.Dividir);
        }
        static void VistaCalculadora(Funcion funSuma, Funcion funResta, Funcion funMultiplicacion, Funcion funDivision)
        {
            Console.WriteLine("¿Cuantos operandos va a utilizar?");
            int operandos = int.Parse(Console.ReadLine());
            List<float> lista = new List<float>(operandos);
            int contar = lista.Count;
            do
            {
                contar++;
                Console.WriteLine("Introduzca el operando " + contar);
                lista.Add(float.Parse(Console.ReadLine()));
            } while (contar < operandos);
            float[] array = lista.ToArray();
            Console.WriteLine("Elija que operación desea realizar: ");
            Console.WriteLine("1.- Suma");
            Console.WriteLine("2.- Resta");
            Console.WriteLine("3.- Multiplicación");
            Console.WriteLine("4.- División");
            float resultado = 0;
            switch (int.Parse(Console.ReadLine()))
            {
                case 1:
                    resultado = funSuma(array);
                    Console.WriteLine("Resultado: " + resultado);
                    break;
                case 2:
                    resultado = funResta(array);
                    Console.WriteLine("Resultado: " + resultado);
                    break;
                case 3:
                    resultado = funMultiplicacion(array);
                    Console.WriteLine("Resultado: " + resultado);
                    break;
                case 4:
                    resultado = funDivision(array);
                    Console.WriteLine("Resultado: " + resultado);
                    break;
                default:
                    Console.WriteLine("Introduzca una opcion valida");
                    break;
            }
        }

    }
}
