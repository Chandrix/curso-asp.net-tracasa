﻿using System;

namespace Wikipedia.Patterns.Strategy
{
    // MainApp Test para aplicacion
    class MainApp
    {
        static void Main()
        {
            Context context;

            // Tres contextos con diferentes estrategias
            context = new Context(new ConcreteStrategyA("Hola que tal"));
            context.Execute();

            context = new Context(new ConcreteStrategyB());
            context.Execute();

            context = new Context(new ConcreteStrategyC("Nombre directo"));
            context.Execute();

        }
    }
    // Una clase abstracta lo principal es que no puede ser instanciada como las interfaces,
    // y puede tener algunos métodos implementados y otros sin implementar(declarando sólo su interfaz). Estos métodos son abstractos.
    abstract class StrategyBase
    {
        protected string nombre;
        public StrategyBase(string mensaje)
        {
            nombre = mensaje + " " + GetType().Name + "Execute";
        }
        protected void RepetirChar(char caracter, int veces)
        {
            for (int i = 0; i < veces; i++)
            {
                Console.Write(caracter);
            }
            Console.Write("\n");
        }
        public abstract void Execute();

        public void MostrarNombre()
        {
            Console.WriteLine(nombre);
        }
    }

    // Implementa el algoritmo usando el patron estrategia
    class ConcreteStrategyA : StrategyBase
    {
        public ConcreteStrategyA(string mensaje) : base(mensaje)
        {
        }

        public override void Execute()
        {
            RepetirChar('-', 30);
            MostrarNombre();
        }

    }

    class ConcreteStrategyB : StrategyBase
    {
        public ConcreteStrategyB() : base("Llamar a")
        {
        }

        public override void Execute()
        {
            RepetirChar('\n', 3);
            Console.WriteLine("Called ConcreteStrategyB.Execute()");
        }
    }

    class ConcreteStrategyC : StrategyBase
    {
        public ConcreteStrategyC(string nombre) : base("")
        {
            this.nombre = nombre;
        }

        public override void Execute()
        {
            RepetirChar('*', 20);
            Console.WriteLine("Called ConcreteStrategyC.Execute()");
        }
    }

    // Contiene un objeto ConcreteStrategy y mantiene una referencia a un objeto Strategy
    class Context
    {
        StrategyBase strategy;

        // Constructor
        public Context(StrategyBase strategy)
        {
            this.strategy = strategy;
        }

        public void Execute()
        {
            strategy.Execute();
        }
    }
}
