﻿using System;

namespace Ejemplo04_Observer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Patron observer: Periodico");
            PeriodicoObservado alDia = new PeriodicoObservado();
            // 1.- Un suscriptor humano.
            SuscriptorHumano juan = new SuscriptorHumano("Juan");
            alDia.AddSuscriptor(juan);
            // 2.- Ocurre una noticia.
            alDia.NotificarNoticia("Un meteorito revienta la Luna", DateTime.Now);

            // 3.- Otro suscriptor humano.
            SuscriptorHumano pedro = new SuscriptorHumano("Pedro");
            alDia.AddSuscriptor(pedro);
            // 4.- Otro suscriptor maquina.
            SuscriptorMaquina maquina1 = new SuscriptorMaquina(085350);
            alDia.AddSuscriptor(maquina1);
            // 5.- Ocurre otra noticia.
            alDia.NotificarNoticia("Un meteorito revienta la Tierra", DateTime.Now);

            // 6.- El otro humano se desuscribe porque dice muchas mentiras.
            // En el periodico tiene que haber un método para quitar suscriptores.
            alDia.BorrarSuscriptor(juan);
            // 7.- Ocurre la última noticia.
            alDia.NotificarNoticia("Un gran meteorito revienta el Sol", DateTime.Now);

            // 8.- El periodico cierra.
            alDia.BorrarPeriodico();
            // 9.- Que los suscriptores reciban también la fecha y hora de la noticia y la muestren.

            // Para que el suscriptor de ciencia se pueda suscribir simplemente se asigna la función al campo delegado:
            alDia.NuevaNoticiaCiencia = NationalGeo;
            alDia.NoticiaCiencia("El meteorito se ha quedado a gusto");
        }

        // Este suscriptor sólo es una función.
        static void NationalGeo(string noticiaDePseudociencia)
        {
            Console.WriteLine(" >>>>> A saber: " + noticiaDePseudociencia);
        }
    }
}
