﻿using System;

namespace Ejemplo04_Observer
{
    class SuscriptorHumano : ISuscriptorObservador
    {
        string nombre;

        public SuscriptorHumano(string nombre)
        {
            this.nombre = nombre;
        }

        public void ActualizarNotificacionNoticia(string noticia, DateTime fecha)
        {
            Console.Beep();
            Console.WriteLine("¡Nuevas noticias, " + nombre + "!");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(noticia);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("En " + fecha);
            Console.ForegroundColor = ConsoleColor.White;
            System.IO.File.AppendAllText("fich_humano.txt", noticia + fecha + "\n");
        }
    }
}
