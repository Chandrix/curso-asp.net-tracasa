﻿using System;

namespace Ejemplo04_Observer
{
    interface ISuscriptorObservador
    {
        void ActualizarNotificacionNoticia(string noticia, DateTime fecha);
    }
}
