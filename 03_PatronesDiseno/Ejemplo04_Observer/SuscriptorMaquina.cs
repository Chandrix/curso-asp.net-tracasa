﻿using System;

namespace Ejemplo04_Observer
{
    class SuscriptorMaquina : ISuscriptorObservador
    {
        int id;

        public SuscriptorMaquina(int id)
        {
            this.id = id;
        }

        public void ActualizarNotificacionNoticia(string noticia, DateTime fecha)
        {
            Console.Beep();
            Console.WriteLine("¡Nuevas noticias, " + id + "!");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(noticia);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("En " + fecha);
            Console.ForegroundColor = ConsoleColor.White;
            System.IO.File.AppendAllText("fich_maquina.txt", noticia + fecha + "\n");
        }

    }
}
