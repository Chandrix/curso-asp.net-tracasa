﻿using System;
using System.Collections.Generic;

namespace Ejemplo04_Observer
{
    delegate void NoticiaCiencia(string not);
    class PeriodicoObservado
    {
        List<ISuscriptorObservador> listaSuscriptores;
        public NoticiaCiencia NuevaNoticiaCiencia;
        public PeriodicoObservado()
        {
            listaSuscriptores = new List<ISuscriptorObservador>();
        }
        public void NoticiaCiencia(string laNoticia)
        {
            if (NuevaNoticiaCiencia != null)
            {
                NuevaNoticiaCiencia("Ciencia: " + laNoticia);
            }
        }
        public void NotificarNoticia(string titular, DateTime fecha)
        {
            Console.WriteLine("¡Extra, extra!");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(fecha.ToString() + " -> " + titular);
            Console.ForegroundColor = ConsoleColor.White;
            foreach (var suscriptor in listaSuscriptores)
            {
                suscriptor.ActualizarNotificacionNoticia(titular, fecha);
            }
            Console.ReadKey();
            Console.Clear();
        }
        public void AddSuscriptor(ISuscriptorObservador observador)
        {
            listaSuscriptores.Add(observador);
        }
        public void BorrarSuscriptor(ISuscriptorObservador observador)
        {
            listaSuscriptores.Remove(observador);
        }
        public void BorrarPeriodico()
        {
            NotificarNoticia("Nos vemos obligados a cerrar debido a la destrucción de nuestro sistema solar.", DateTime.Now);
            listaSuscriptores.Clear();
            System.IO.File.Delete("fich_humano.txt");
            System.IO.File.Delete("fich_maquina.txt");
        }

    }
}
