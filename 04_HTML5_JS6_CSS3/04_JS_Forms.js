/* Javascript no tiene clases. Es un lenguaje prototípico, es decir,
basado en prototipos. */
window.onload = function(){
    let btnAnadir = document.getElementById("btn-anadir");
    let usuario = JSON.parse(window.localStorage.getItem("mi-usuario"));
    console.log("Cargado" + JSON.stringify(usuario));
    let filatabla = `
    <tr><td>${usuario.nombre}</td>
    <td>${usuario.edad}</td>
    <td>${usuario.altura}</td></tr>`
    document.getElementById("tbody-usuarios").innerHTML = filatabla;
   
    btnAnadir.onclick = function(){
        let campoNombre = document.getElementById("nombre");
        campoNombre.value = campoNombre.value.toLowerCase();
        let nombre = campoNombre.value;
        let edad = parseInt(document.getElementById("edad").value);
        let altura = parseFloat(document.getElementById("altura").value);
       /* let edadSiguienteDecada = edad + 10;
        alert(`¡Yeeeepa" ${nombre}, tienes ${edad} años 
        y la siguiente decada tendrás ${edadSiguienteDecada}!`); */
        // Aquí copiamos la instancia del Object prototipo.
        /*let usuario = new Object();
        usuario.nombre = nombre;
        usuario.edad = edad;
        usuario.altura = altura; */

        let aficiones = new Object();
        aficiones.leer = true;
        aficiones.musica = true;
        aficiones.cine = true;

        let usuario = {
            "nombre" : nombre,
            "edad" : edad,
            "altura" : altura,
            "aficiones" : aficiones
        }
        alert(textoAlertUsuario(usuario));
        window.localStorage.setItem("mi-usuario", JSON.stringify(usuario));
        
        // Utilizamos notación JSON (Javascript Object Notation)
        /*let aficiones = {   // Las llaves son como new Object();
                "cine": true,
                "leer": true,
                "musica": false*/
        
        usuario.aficiones = aficiones;
    };
    function textoAlertUsuario(usu){
        return `¡Hola ${usu.nombre}, tienes ${usu.edad} años 
        y mides ${usu.altura} metros!. Te gusta ${usu.aficiones.leer ? "leer" :
        ""} ${usu.aficiones.musica ? "musica" : ""} y 
        ${usu.aficiones.cine ? "el cine" : ""}`; 
    }
    document.getElementById("btn-ir").addEventListener("click", function(){
        window.location = document.getElementById("url").value;
    })
};