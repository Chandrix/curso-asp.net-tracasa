﻿using System;

namespace Ejemplo_Paso_Valor_Ref
{
    class Program
    {
        static void Main(string[] args)
        {
            int variableEnt = 10;
            Console.WriteLine("Entero fuera y antes: " + variableEnt);
            RecibimosUnValor(variableEnt);
            Console.WriteLine("Entero fuera y despues: " + variableEnt);
            RecibimosUnaReferencia(ref variableEnt);
            Console.WriteLine("Entero fuera y despues: " + variableEnt);
        }
        static void RecibimosUnValor(int entero)
        {
            entero = entero * 2 + 1;
            Console.WriteLine("Entero dentro y despues: " + entero);
            entero = entero * 2 + 1;
            Console.WriteLine("Entero dentro y despues: " + entero);
        }
        static void RecibimosUnaReferencia(ref int entero)
        {
            entero = entero * 2 + 1;
            Console.WriteLine("Entero dentro y despues: " + entero);
            entero = entero * 2 + 1;
            Console.WriteLine("Entero dentro y despues: " + entero);
        }
    }
}
