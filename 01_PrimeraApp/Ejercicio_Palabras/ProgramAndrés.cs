﻿using System;

namespace Ejercicio_Palabras
{
    class Program
    {
        static void Main(string[] args)
        {
            // Ejercicio: Pedir al usuario 3 palabras y juntarlas en una única variable de tipo texto
            // separandolas por comas y sin espacios
            /* "palabra"
             * "otra    palabra"
             * "  tercera palabra"
             * "cuarta             palabra"
             * Resultado: "palabra, otra palabra, tercera palabra, cuarta palabra"
             */

            Console.WriteLine("Escriba la primera palabra");
            string palabra1 = (Console.ReadLine());
            Console.WriteLine("Escriba la segunda palabra");
            string palabra2 = (Console.ReadLine());
            Console.WriteLine("Escriba la tercera palabra");
            string palabra3 = (Console.ReadLine());
            Console.WriteLine("Escriba la cuarta palabra");
            string palabra4 = (Console.ReadLine());

            string [] palabras = { palabra1.Replace("  ", string.Empty), palabra2.Replace("  ", string.Empty), palabra3.Replace("  ", string.Empty), palabra4.Replace("  ", string.Empty)};
            var resultado = string.Join(", ", palabras);
            Console.WriteLine("Resultado:"+ resultado);
        }
    }
}
