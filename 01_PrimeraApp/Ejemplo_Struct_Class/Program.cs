﻿using System;

namespace Ejemplo_Struct_Class
{
    /* Crear una estructura ProductoE independiente (No anidada) con nombre, precio y su constructor, con una función para mostrar sus datos
     * 
     * Crear una clase ProductoC independiente (No anidada) con nombre, precio y su constructor, con una función para mostrar sus datos
     * 
     * Crear 4 funciones estaticas en Program:
     *  - Una que reciba ProductoE y modifique su nombre y su precio
     *  - Otra que reciba una ref ProductoE y modifique su nombre y su precio
     *  - Una que reciba ProductoC y modifique su nombre y su precio
     *  - Otra que reciba una ref ProductoC y modifique su nombre y su precio
     
     Por último, en Main(), comprobar el comportamiento para saber cual modifica realmente la variable original
     */

    /* - Estructuras
     * 1.- Las estructuras siempre se pasan por valor a menos que se indique a posta con ref que es por referencia.
     * 2.- No pueden heredar unas de otras.
     * 3.- No puede haber estructuras estáticas.
     * 4.- Siempre siempre tienen un constructor por defecto.
     */

    /* - Clases
     * 1.- Las clases siempre se pasan por referencia, es redundante usar ref.
     * 2.- Sí pueden heredar unas clases de otras.
     * 3.- Sí puede haber clases estáticas.
     * 4.- Sólo tienen un constructor por defecto cuando no hemos creado un constructor explícitamente.
     */
    struct ProductoE
    {
       public string nombre;
       public float precio;

        public ProductoE(string nombre, float precio)
        {
            this.nombre = nombre;
            this.precio = precio;
        }
        public void DatosProductoE()
        {
            Console.WriteLine("Nombre: ");
            Console.WriteLine(nombre);
            Console.WriteLine("Precio");
            Console.WriteLine(precio);
        }
    }
    class ProductoC
    {
        public string nombre;
        public float precio;

        public ProductoC(string nombre, float precio)
        {
            this.nombre = nombre;
            this.precio = precio;
        }
        public void DatosProductoE()
        {
            Console.WriteLine("Nombre: ");
            Console.WriteLine(nombre);
            Console.WriteLine("Precio");
            Console.WriteLine(precio);
        }
    }

    class Program
    {
        static void ModificarProductoE(string ProductoE)
        {
            ProductoE;
            ProductoE = ProductoE.ToUpper();
        }

        static void ModificarRefProductoE(ref string ProductoE)
        {

            ProductoE.nombre= ;
        }

        static void ModificarProductoC(string ProductoC)
        {
            ProductoC = ProductoC.ToUpper();

        }

        static void ModificarRefProductoC(ref string ProductoC)
        {
            ProductoC = ProductoC.ToUpper();

        }
        /*static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }*/
    }
}
