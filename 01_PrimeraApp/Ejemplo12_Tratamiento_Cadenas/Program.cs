﻿using System;

namespace Ejemplo12_Tratamiento_Cadenas
{
    class Program
    {
        static void Main(string[] args)
        {
            string texto = "       En un lugar de La Mancha de cuyo nombre no quiero acordarme        ";
            Console.WriteLine("Original: " + texto);
            Console.WriteLine("Sin espacios: " + texto.Trim());
            Console.WriteLine("Mayus" + texto.ToUpper());
            Console.WriteLine("Minus" + texto.ToLower());
            Console.WriteLine("Un cacho" + texto.Substring(20, 20));
            Console.WriteLine("Un cacho hasta el final" + texto.Substring(20));
            Console.WriteLine("Un cacho hasta el final" + texto.Substring(20));
            Console.WriteLine("La Mancha" + texto.IndexOf("La Mancha"));

            string[] palabras = texto.Trim().Split(" ");
            Console.WriteLine("Palabra por palabra: ");
            for (int p = 0; p < palabras.Length; p++) {
                Console.WriteLine("Palabra" + p + ":" + palabras[p]);
        }
        Console.WriteLine("Por Pamplona:" + texto.Replace("La Mancha", "Pamplona").Replace("no","Sarriguren"));
        
        }
}
}
// Ejercicio: Pedir al usuario 3 palabras y juntarlas en una única variable de tipo texto
// separandolas por comas y sin espacios
/* "palabra"
 * "otra    palabra"
 * "  tercera palabra"
 * "cuarta             palabra"
 * Resultado: "palabra, otra palabra, tercera palabra, cuarta palabra"
 */
