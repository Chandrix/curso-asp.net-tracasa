﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo01_Encapsulacion
{
    public interface IMostrarPedir
    {
        public interface MostrarPedir
        {
            void MostrarDatos();

            void PedirDatos();
        }
    }
}
