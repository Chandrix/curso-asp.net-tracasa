﻿using Ejemplo01_Encapsulacion;
using Ejemplo02_Herencia;
using Ejemplo03_Interfaces;
using Ejercicio01_Encapsulacion;
using Ejercicio02_Herencia;
using System;

namespace Ejercicio03_Interfaces
{/*1.-  Comprobar si Coche Electrico puede hacerse polimorfismo con INombrable. 🗸
 * 2.-  Podeis un nuevo proyecto Ejercicio03_interfaces.
 *      Hacer que el usuario que ya tenemos implemente la interfaz 
 *      INombrable y usar los 2 métodos y la propiedad. 🗸
 * 3.-  Crear un array de INombrable con un empleado y su coche electrico.
 * 4.-  ICloneable YA EXISTE en .Net.
 *      Implementar dicha interfaz en CocheElectrico:
 *      El método tiene que instanciar un nuevo obj y asignar.
 *      las propiedades del nuevo coche con sus propias propiedades. 🗸 
 * 5.-  Crear una nueva interfaz que obligue a implementar 2 métodos, uno para mostrar directamente los datos por consola y otro para pedir sus datos por consola.
 * 6.-  Implementar dicha interfaz en Usuario y en Coche.
 * 7.-  Usar los métodos en un nuevo usuario y en el empleado del ejercicio 3, y en un Coche (y si quereis en CocheElectrico).
 */
    class Program
    {
        static void Main(string[] args)
        {
            object[] cosas = new object[3];
            cosas[0] = new Coche("Mercedes", "Gato", 20000);
            cosas[1] = new CocheElectrico("Citroen", "Saxo", 15000);
            cosas[2] = new Usuario("Aquiles", 30, 1.80f);
            // ((Empleado)cosas[0]).SuCoche = (Coche)cosas[1];
            // 1.-
            ((CocheElectrico)cosas[1]).SetNombre("Seat - Coma 6.0");
            CocheElectrico seatComa = (CocheElectrico)cosas[1];
            Console.WriteLine(seatComa.GetNombre());


            foreach (INombrable innom in cosas)
            {
                Console.WriteLine(innom.ToString());
            }

            // 2.-
            Usuario usuario1 = (Usuario)cosas[2];
            Console.WriteLine(usuario1.GetNombre());

            // 3.-
            INombrable[] EmpleadoElectrico = new INombrable[2];
            EmpleadoElectrico[0] = new Empleado("Hector", 32, 1.8f, 2000);
            EmpleadoElectrico[1] = new CocheElectrico("BMW", "X5", 15000, 90);
           // (Empleado)EmpleadoElectrico[0].SuCoche = (Coche)EmpleadoElectrico[1];


            // 4.-
            Console.WriteLine(seatComa.Clone());
            CocheElectrico mismoElectrico = (CocheElectrico)cosas[1];
            CocheElectrico copiaElectrico = (CocheElectrico)((CocheElectrico)cosas[1]).Clone();

            mismoElectrico.Modelo = "Nuevo modelo S2";
            Console.WriteLine("mismoElectrico = " + mismoElectrico.ToString());
            Console.WriteLine("varios[1] = " + cosas[1].ToString());

            copiaElectrico.Modelo = "Copia nuevo modelo S2";
            Console.WriteLine("copiaElectrico = " + copiaElectrico.ToString());
            Console.WriteLine("varios[1] = " + cosas[1].ToString());


            // 5.- 
            Empleado empleadoUsuario = new Empleado();
            empleadoUsuario.PedirDatos();
            empleadoUsuario.MostrarDatos();

            Coche cocheUsuario = new Coche();
            cocheUsuario.PedirDatos();
            cocheUsuario.MostrarDatos();

        }
    }
}
