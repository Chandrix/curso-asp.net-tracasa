﻿using Ejemplo01_Encapsulacion;
using Ejercicio01_Encapsulacion;
using Ejemplo02_Herencia;
using System;
using System.Collections.Generic;

namespace Ejemplo03_Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            // Por lo general no es aconsejable crear un array de objetos.
            // Ojo: Esto es sólo a nivel educativo.
            // Lo normal es declarar un array de clases, interfaces ó  clases abstractas.
            object[] popurri = new object[3];
            popurri[0] = new Coche("Fiat", "Punto", 9000);
            popurri[1] = new CocheElectrico("Fiat", "Punto", 9000);
            popurri[2] = new Usuario("Fulanito", 50, 2);

            foreach (object objQueSea in popurri)
            {
                Console.WriteLine(objQueSea.ToString());
            }
            ((Coche)popurri[0]).SetNombre("FIAT - PUNTO 4.5");
            Coche fiatPunto = (Coche) popurri[0];
            Console.WriteLine(fiatPunto.GetNombre());
            // El polimorfismo se puede usar con interfaces
            INombrable fiatNombrable = (INombrable) fiatPunto;
            fiatNombrable.Nombre = "Fiat - Punto Version 1034";
            Console.WriteLine(fiatNombrable.Nombre);

            INombrable cen = (CocheElectrico)popurri[1];
            Console.WriteLine(cen.Nombre);
            EjemploLista();
        }
        static void EjemploLista()
        {
            List<string> textos = new List<string>();
            //Por defecto se crean 10 o 20 elementos, pero esto nos da igual, lo hace de manera interna. Podemos especificar el tamaño inicial (3).
            textos.Add("Primer texto");
            textos.Add("Segundo texto");
            textos.Add("Tercer texto");
            textos.Add("Cuarto texto");
            for (int i = 0; i < 30; i++)
                textos.Add("Texto " + i + "º ");
            // Llegará un punto en el que internamente tenga que ampliar dinámicamente.
            textos.RemoveAt(2); // Y al eliminar tendrá que reestructurar internamente.
            textos.Remove("Otro texto"); // Estas operaciones cuestan trabajo.
            foreach (string x in textos)
                Console.WriteLine("Lista: " + x);

            List<Coche> coches = new List<Coche>();
            coches.Add(new Coche("Toyota", "Cupra", 50000));
            coches.Add(new CocheElectrico("Tesla", "F21", 40000));
            IList<Coche> icoches = coches;
            icoches.Add(new Coche());
            IList<Usuario> listausuarios = new List<Usuario>();
            listausuarios.Add(new Usuario("Usuario de Lista 1", 0, 0));
            listausuarios.Add(new Usuario("Usuario de Lista 2", 0, 0));
            IList<Usuario> arrayusuarios = new Usuario[10];
            arrayusuarios[0] = new Usuario("Usuario de Lista 2", 0, 0);
            arrayusuarios[1] = new Usuario("Usuario de Lista 2", 0, 0);
            arrayusuarios[5] = new Usuario("Usuario de Lista 5", 0, 0);
            MostrarColeccion(listausuarios);
            MostrarColeccion(arrayusuarios);
        }
        static void MostrarColeccion(ICollection<Usuario> icoleccion)
        {
            Console.WriteLine("Coleccion usuarios" + icoleccion.GetType());
            foreach(Usuario usuario in icoleccion)
            {
                if (usuario != null)
                usuario.MostrarDatos();
                // Esta es una abreviatura de la anterior: El ? antes de un método pregunta si es null.
                //usuario?.MostrarDatos();
            }
        }
    }
}
/* 1 - Comprobar si Coche Electrico puede hacerse polimorfismo con INombrable.
 * 2 - Podeis un nuevo proyecto Ejercicio03_interfaces.
 *      Hacer que el usuario que ya tenemos implemente la interfaz 
 *      INombrable y usar los 2 métodos y la propiedad.
 * 3 -  Crear un array de INombrable con un empleado y su coche electrico.
 * 4 -  Implementar la interfaz IClonable en CocheElectrico:
 *      El método tiene que instanciar un nuevo obj y asignar.
 *      las propiedades del nuevo coche con sus propias propiedades.
 */