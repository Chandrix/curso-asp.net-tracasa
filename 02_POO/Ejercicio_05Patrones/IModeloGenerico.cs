﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio_05Patrones
{
    interface IModeloGenerico<Tipo>
    {
        Tipo Crear(Tipo nuevoObj);

        IList<Tipo> LeerTodos();

        Tipo LeerUno(int entero);

        bool Eliminar(int entero);

        // Tipo Modificar(Tipo nuevoObj);
    }
}
