﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio_05Patrones
{
    class ControladorMuestra
    {

        IModeloEjemplo modelo;
        IVistaEjemplo vista;
        //TODO: Campo IVista

        public ControladorMuestra(IModeloMuestra modelo)
        {
            this.modelo = modelo;
            // TODO: Y recibir vista.
        }
        public Muestra AltaEjemplo(int entero, string str)
        {
            // 1.- Recibimos el parametro del ejemplo, probablemente de la vista.
            // 2.- Guardamos el ejemplo en el modelo.
            return modelo.Crear(entero, str);
        }

        public IEnumerable<Muestra> LeerEjemplos()
        {
            return modelo.LeerTodos();
        }

        public Muestra LeerUno(string nombre)
        {
            return modelo.LeerUno(nombre);
        }
        public Muestra LeerUno(int entero)
        {
            return modelo.LeerUno(entero);
        }
        public bool EliminarUno(string nombre)
        {
            return modelo.Eliminar(nombre);
        }

        internal Muestra Modificar(string nombre, int entero, string str)
        {
            throw new NotImplementedException();
        }
    }
}
