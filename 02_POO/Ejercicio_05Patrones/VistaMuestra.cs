﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio_05Patrones
{
    class VistaMuestra
    {
        ControladorMuestra controlador;

        public VistaMuestra(ControladorMuestra controlador)
        {
            this.controlador = controlador;
        }
        public void Menu()
        {
            int opcion = 0;
            do
            {
                Console.WriteLine("MENU: ( 0 -Salir)");
                Console.WriteLine(" 1 - Alta ejemplo");
                Console.WriteLine(" 2 - Mostrar todos");
                Console.WriteLine(" 3 - Mostrar uno");
                string str_op = Console.ReadLine();
                if (int.TryParse(str_op, out opcion))
                {
                    switch (opcion)
                    {
                        case 1:
                            AltaEjemplo();
                            break;
                        case 2:
                            MostrarEjemplos();
                            break;
                        case 3:
                            MostrarUno();
                            break;
                        default:
                            Console.WriteLine("Introduzca opcion valida");
                            break;
                    }
                }
                else
                {
                    opcion = -1;
                    Console.WriteLine("Escribe bien el número");
                }
            } while (opcion != 0);
        }
        public void AltaEjemplo()
        {
            Console.WriteLine("Alta ejemplo: numero: ");
            int entero = int.Parse(Console.ReadLine());
            Console.WriteLine("Alta ejemplo: string: ");
            string str = Console.ReadLine();
            this.controlador.AltaEjemplo(entero, str);
        }
        public void MostrarEjemplos()
        {
            IEnumerable<Muestra> todos = controlador.LeerEjemplos();
            foreach (Muestra ejemplo in todos)
            {
                Console.WriteLine("Ejemplo " + ejemplo.ToString());
            }
        }
        public void MostrarUno()
        {
            Console.WriteLine(" 1 - Busqueda por numero ");
            Console.WriteLine(" 2 - Busqueda por nombre ");
            int opcion;
            string op = Console.ReadLine();
            if (int.TryParse(op, out opcion))
            {
                switch (opcion)
                {
                    case 1:
                        {
                            Console.WriteLine("Introduce el numero: ");
                            int numero = int.Parse(Console.ReadLine());
                            Muestra ejemplo = controlador.LeerUno(numero);
                            if (ejemplo != null)
                                Console.WriteLine("Ejemplo " + ejemplo.ToString());
                            else
                                Console.WriteLine("No encontrado por " + numero);
                            break;
                        }
                    case 2:
                        {
                            Console.WriteLine("Introduce el nombre: ");
                            string nombre = Console.ReadLine();
                            Muestra ejemplo = controlador.LeerUno(nombre);
                            if (ejemplo != null)
                                Console.WriteLine("Ejemplo " + ejemplo.ToString());
                            else
                                Console.WriteLine("No encontrado por " + nombre);
                            break;
                        }
                }
            }
        }
        public void ModificarUno()
        {
            Console.WriteLine("Intro nombre a buscar: ");
            string nombre = Console.ReadLine();
            if (controlador.LeerUno(nombre) == null)
            {
                Console.WriteLine("No se ha encontrado " + nombre);
            }
            else
            {
                Console.WriteLine("Nuevo numero:");
                int entero = int.Parse(Console.ReadLine());
                Console.WriteLine("Nuevo string:");
                string str = Console.ReadLine();
                Muestra mu = this.controlador.Modificar(nombre, entero, str);

                if (mu != null)
                    Console.WriteLine("Modificado " + ej.ToString());
                else
                    Console.WriteLine("No encontrado por " + nombre);
            }
        }

    }
}
