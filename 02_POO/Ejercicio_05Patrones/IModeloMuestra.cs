﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio_05Patrones
{
    interface IModeloMuestra : IModeloGenerico<Muestra>
    {
        Muestra Crear(int entero, string str);

        IList<Muestra> LeerTodos();
        public Muestra LeerUno(string nombre);

        public bool Eliminar(string nombre);

        public Muestra Modificar(string nombreBusq, int entero, string str);


        public Muestra LeerUno(int entero);
        IList<Muestra> LeerTodos(string nombre);
    }
}
