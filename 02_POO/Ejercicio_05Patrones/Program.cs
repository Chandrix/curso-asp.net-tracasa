﻿using System;

namespace Ejercicio_05Patrones
{
    class Program
    {
        IModeloMuestra model1;
        VistaMuestra ve;
        Program()
        {
            this.model1 = new ModeloMuestra();
            ControladorMuestra controlador = new ControladorMuestra(model1);
            this.ve = new VistaMuestra(controlador);
        }
        void ProbarDatos()
        {
            ModeloMuestra model1 = new ModeloMuestra();
            model1.Crear(1, "Uno");
            model1.Crear(2, "Dos");
            model1.Crear(3, "Tres");
        }
        static void Main(string[] args)
        {
            Program program = new Program();
            program.ProbarDatos();
            program.ProbarDatos();
            program.ve.Menu();
        }
    }
}
