﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio_05Patrones
{
    class ModeloMuestra : IModeloMuestra // IModeloGenerico<Muestra>
    {
        private List<Muestra> muestras;

        public ModeloMuestra()
        {
            muestras = new List<Muestra>();
        }

        IList<Muestra> IModeloGenerico<Muestra>.LeerTodos()
        {
            return LeerTodos();
        }
        public Muestra Crear(Muestra muestra)
        {
            muestras.Add(muestra);
            return muestra ;
        }
        public bool Eliminar(string nombre)
        {
            Muestra ejemplo = LeerUno(nombre);
            if (ejemplo != null)
            {
                return muestras.Remove(ejemplo);
            }
            return false;
        }
        public bool Eliminar(int entero)
        {
            Muestra ejemplo = LeerUno(entero);
            if (ejemplo != null)
            {
                return Muestra.Remove(ejemplo);
            }
            return false;
        }
        public List<Muestra> LeerTodos()
        {
            return muestras;
        }
        public Muestra LeerUno(string str)
        {
            foreach (Muestra ejemplo in muestras)
            {
                if (ejemplo.Str == str)
                    return ejemplo;
            }
            return null;
        }
        public Muestra LeerUno(int entero)
        {
            foreach (Muestra ejemplo in muestras)
            {
                if (ejemplo.EsEntero(entero))
                    return ejemplo;
            }
            return null;
        }

        public Muestra Crear(int entero, string str)
        {
            return Crear(new Muestra(entero, str));
        }

        IList<Muestra> IModeloMuestra.LeerTodos(string nombre)
        {
            return LeerTodos();
        }

        public Muestra Modificar(string nombreBusq, int entero, string str)
        {
            Muestra usuarioamodif = LeerUno(nombreBusq);
            if (usuarioamodif == null)
                return null;
            int posicion = muestras.IndexOf(usuarioamodif);
            muestras[posicion] = new Muestra(entero, str);
            return muestras[posicion];
        }
        // De esta manera creamos al final de la lista, alterando el orden.
        public Muestra Modificar2(string nombreBusq, int entero, string str)
        {
            Eliminar(nombreBusq);
            return Crear(entero, str);
        }

        IList<Muestra> IModeloMuestra.LeerTodos()
        {
            return LeerTodos();
        }
    }
}
