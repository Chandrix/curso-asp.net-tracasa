﻿using System;

namespace Ejemplo06_CSharp_Avanzado
{
    class Program
    {
        static void Main(string[] args)
        {
            Fruta pera = new Fruta("Pera", 150f);
            Console.WriteLine(pera.ToString());
            Console.WriteLine(pera.FormatearNombre());
            Console.WriteLine(OtroModulo.FormatearNombreN(pera));
            int[] otroarray = new int[] { 3, 2, 1 };
            Console.WriteLine(otroarray.ToUpperString());
            string[] otroarrayStr = new string[] { "tres", "dos", "uno" };
            Console.WriteLine(otroarray.ToUpperString());

            // Ejercicio: Método de extensión de array, que sea array.ToStringFiltrado("texto");
            // Que sólo muestre los ToString() que contengan cadena, sin tener en cuenta mayusculas o minusculas.
            // Ej: otroArrayStr.ToStringFiltrado("s") tres, dos, uno
            // Ej: frutas.ToStringFiltrado("p") Pera, Piña, Platano.
            Fruta[] frutas = new Fruta[]
            {
                new Fruta ("Manzana", 175),
                new Fruta ("Piña", 375),
                new Fruta ("Platano", 125),
                new Fruta ("Chirimoya", 275)
            };
        }
    }
    // En C# podemos añadir métodos a una clase desde otras clases

    public static class OtroModulo
    {
        // Mediante una función estática, con esta estructura.
        public static string FormatearNombre(this Fruta laFruta)
        {
            // Método de extensión.
            string nombreFormateado = "Fruta: " + laFruta.Nombre.ToUpper();
            return nombreFormateado;
        }
        public static string FormatearNombreN(this Fruta laFruta)
        {
            // Método de extensión.
            string nombreFormateado = "Fruta: " + laFruta.Nombre.ToUpper();
            return nombreFormateado;
        }
        public static string FormatearNombre(this Fruta laFruta, string formato)
        {
            // Método de extensión.
            return formato + " " + laFruta.Nombre.ToUpper();
        }
        public static string ToUpperString(this object cualquierObjeto)
        {
            // Método de extensión.
            return cualquierObjeto.ToString().ToUpper();
        }
        public static string ToUpperString(this Array cualquierArray)
        {
            // Método de extensión.
            string result = "Array " + cualquierArray.ToString().ToUpper();
            foreach (object elem in cualquierArray)
            {
                result += "\n - " + elem.ToUpperString();
            }
            return result;
        }
        public static string ToStringFiltrado(this Array cualquierArray, string cadenaFiltro)
        {
            // Método de extensión.
            string result = "Array " + cualquierArray.ToString().ToUpper();
            foreach (object elem in cualquierArray)
            {
                if (elem.ToString().ToUpper().Contains(cadenaFiltro.ToUpper()))
                    result += "\n - " + elem.ToUpperString();
            }
            return result;
        }
    }
}
