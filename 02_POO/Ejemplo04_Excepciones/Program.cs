﻿using System;

namespace Ejemplo04_Excepciones
{
    class Program
    {
        static void FuncionRecursivaInfinita(int i)
        {
            if (i < 100)
                FuncionRecursivaInfinita(i++);
        }
        static void Main(string[] args)
        {
            // Excepciones típicas existentes en .Net (y en otros lenguajes)
            // Stack Overflow:
            try
            {
                // No queda otra que evitarlo
                FuncionRecursivaInfinita(200);
            } catch (StackOverflowException ex)
            {
                Console.Error.WriteLine("¡Error! " + ex.Message);
            }
            // CAPTURAR ERROR DE MANERA ESPECÍFICA
            try
            {
                Console.WriteLine("Convirtiendo lo inconvertible!");
                int.Parse("No numero");
                Console.WriteLine("Esta linea ni se muestra");
            }
            catch (FormatException ex)  // Cuando ocurre una excepción en control pasa a los bloques catch
            {
                Console.Error.WriteLine("¡Error! " + ex.Message);
            }
            // CAPTURAR ERROR DE MANERA GENÉRICA (Exception)
            try
            {
                Console.WriteLine("Convirtiendo lo inconvertible!");
                int.Parse("No numero");
                Console.WriteLine("Esta linea ni se muestra");
            }
            catch (Exception ex)  // Cuando ocurre una excepción en control pasa a los bloques catch
            {
                Console.Error.WriteLine("¡Error! " + ex.Message);
            }
            // CAPTURAR OTRO ERROR DE MANERA GENÉRICA (Exception)

            try
            {
                Console.WriteLine("Crear un objeto nulo y tratar de usarlo");
                object nadaYmenos = null;
                Console.WriteLine(nadaYmenos.ToString());
            }
            catch (Exception ex)  // Cuando ocurre una excepción en control pasa a los bloques catch
            {
                Console.Error.WriteLine("¡Error! " + ex.Message);
            }
            // NO CAPTURAR OTRO ERROR 
            try
            {
                Console.WriteLine("Crear un objeto nulo y tratar de usarlo");
                object nadaYmenos = null;
                Console.WriteLine(nadaYmenos.ToString());
            }
            catch (FormatException ex)  // Cuando ocurre una excepción en control pasa a los bloques catch
            {
                Console.Error.WriteLine("¡Error! " + ex.Message);
            }
            catch (SystemException ex)  // Cuando ocurre una excepción en control pasa a los bloques catch
            {
                Console.Error.WriteLine("¡Error! " + ex.Message);
            }
            catch (Exception ex)  // Cuando ocurre una excepción en control pasa a los bloques catch
            {
                Console.Error.WriteLine("¡Error! " + ex.Message);
            }
            // Provocar una excepción con throw
            try
            {
                throw new FormatException("Error de formato porque me da la gana");
            }
            catch (FormatException ex)  // Cuando ocurre una excepción en control pasa a los bloques catch
            {
                Console.Error.WriteLine("¡Error! " + ex.Message);
            }
            try
            {
                FuncionQueDelegaExcepcion();
                // La excepción se propaga siempre hacia arriba hasta
                // que un try catch adecuado la controle
            }
            catch (FormatException ex)  // Cuando ocurre una excepción en control pasa a los bloques catch
            {
                Console.Error.WriteLine("¡Error desde abajo! " + ex.Message);
            }
        }

        public static void OtraFuncionQueDelegaExcepcion()
        {
            // Si no se controla, se propaga hacia arriba
            try { FuncionQueDelegaExcepcion(); } catch (NullReferenceException ex) { }            
        }
        public static void FuncionQueDelegaExcepcion()
        {
            // Si una función lanza un excepción, y no la controlamos aquí,
            // podrá o deberá ser controlada en la función que invoca a esta
            int.Parse("¡Error!");
        }
    }
}
