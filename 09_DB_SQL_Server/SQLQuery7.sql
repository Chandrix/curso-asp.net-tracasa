﻿-- El conjunto común de datos 
-- SELECT U.Id, U.Email, C.Marca FROM USUARIO AS U INNER JOIN COCHE AS C ON U.idCoche = C.Id

-- Todos los usuarios y si tienen coche, con su coche. Y si no, se rellena lo del coche como NULL
-- SELECT U.Id, U.Email, C.Marca FROM USUARIO AS U LEFT JOIN COCHE AS C ON U.idCoche = C.Id

-- Todos los coches con sus usuarios si los tuviera, y si no también
-- SELECT U.Id, U.email, C.Id, C.Marca FROM Usuario as U RIGHT JOIN Coche as C ON U.idCoche = C.Id

-- Todo con todo aunque no tengan combinación
-- SELECT U.Id, U.email, C.Id, C.Marca FROM Usuario as U FULL OUTER JOIN Coche as C ON U.idCoche = C.Id

	 /*select SUM(c.Cantidad) from Producto as P, CompraUsuarioProducto as C, Usuario as U 
			where C.IdUsuario = U.Id and C.IdProducto = P.Id and U.edad <= 20 
		1 - Esta consulta con JOIN */
		SELECT SUM(C.Cantidad) AS 'CantidadTotal' FROM Producto AS P INNER JOIN CompraUsuarioProducto AS C ON P.Id = C.IdProducto 
		INNER JOIN Usuario AS U ON C.IdUsuario = U.Id WHERE U.Edad < 20

		-- Por cada usuario, nombre de usuario y nombre de producto,
		-- 2 - El precio del producto de sus compras
		SELECT U.Nombre, SUM(P.Precio) AS 'PrecioCompras' FROM Usuario AS U INNER JOIN CompraUsuarioProducto AS C ON U.Id = C.IdUsuario 
		INNER JOIN Producto AS P ON P.Id = C.IdProducto GROUP BY U.Nombre

		-- 3 - Cuanto se ha gastado en total en cada producto
		SELECT U.Nombre, P.Nombre, SUM(P.Precio*C.Cantidad) AS 'PrecioCompras' FROM Usuario AS U INNER JOIN CompraUsuarioProducto AS C ON U.Id = C.IdUsuario 
		INNER JOIN Producto AS P ON P.Id = C.IdProducto GROUP BY P.Nombre, U.Nombre

		-- 4 - Cuantas copias del producto ha comprado
		SELECT U.Nombre, P.Nombre, SUM(C.Cantidad) AS 'Copias' FROM Usuario AS U INNER JOIN CompraUsuarioProducto AS C ON U.Id = C.IdUsuario 
		INNER JOIN Producto AS P ON P.Id = C.IdProducto GROUP BY U.Nombre, P.Nombre ORDER BY U.Nombre

	-- Por otro lado:
		-- 5 - Todas las ventas (dinero) del 2020
		SELECT SUM(P.Precio*C.Cantidad) AS 'Ventas' FROM Usuario AS U INNER JOIN CompraUsuarioProducto AS C ON U.Id = C.IdUsuario 
		INNER JOIN Producto AS P ON P.Id = C.IdProducto WHERE C.Fecha BETWEEN '20200101'  AND '20201231'

		-- 6 - Todos los usuarios que compraron en el 2019
		SELECT DISTINCT U.Nombre FROM Usuario AS U INNER JOIN CompraUsuarioProducto AS C ON U.Id = C.IdUsuario
		WHERE C.Fecha BETWEEN '20190101' AND '20191231'

		-- 7 - Qué productos se han vendido mejor a lo largo del 2018, 2019 y 2020
		SELECT TOP 3 P.Nombre, SUM(C.Cantidad) AS 'Ventas' FROM Usuario AS U INNER JOIN CompraUsuarioProducto AS C ON U.Id = C.IdUsuario 
		INNER JOIN Producto AS P ON P.Id = C.IdProducto WHERE C.Fecha BETWEEN '20180101'  AND '20201231' GROUP BY P.Nombre 
		ORDER BY SUM(C.Cantidad) DESC

		-- 8 - El coche cuyo propietario ha comprado más a lo largo de la historia
		SELECT C.Marca, C.Modelo FROM (SELECT TOP 1 U.Id , SUM(P.Precio*C.Cantidad) AS 'PrecioTotal' FROM Producto AS P INNER JOIN CompraUsuarioProducto AS C 
		ON P.Id = C.IdProducto INNER JOIN Usuario AS U ON U.Id = C.IdUsuario WHERE U.idCoche IS NOT NULL GROUP BY U.Id ORDER BY PrecioTotal DESC) AS S, Coche AS C 
		INNER JOIN Usuario AS U ON C.Id =U.idCoche WHERE U.Id = S.Id

		-- 9 - La media de edad de los compradores de cada uno de los productos (melón, cuajada...)
		SELECT P.Nombre, AVG(U.Edad) 'MediaEdad' FROM Usuario AS U INNER JOIN CompraUsuarioProducto AS C ON C.IdUsuario = U.Id INNER JOIN Producto AS P 
		ON P.Id = C.IdProducto WHERE C.IdUsuario = U.Id GROUP BY P.Nombre

		-- 10 - Qué productos han comprado los usuarios del Mercedes
		SELECT DISTINCT U.Nombre, P.Nombre FROM (SELECT U.Id AS 'UsuariosMercedes' FROM Usuario AS U INNER JOIN Coche AS C 
		ON U.idCoche = C.Id WHERE C.Marca = 'Mercedes') AS S, Coche AS C 
		INNER JOIN Usuario AS U ON C.Id =U.idCoche INNER JOIN CompraUsuarioProducto AS CUP ON U.Id = CUP.IdUsuario 
		INNER JOIN Producto AS P ON P.Id = CUP.IdProducto WHERE U.Id = S.UsuariosMercedes
