﻿SET IDENTITY_INSERT [dbo].[Usuario] ON
INSERT INTO [dbo].[Usuario] ( [Email], [Nombre], [Edad], [Altura], [Activo]) VALUES (N'pabdfsaflo@gilimail.com', N'Pablo', 12, 1.3, 1)
INSERT INTO [dbo].[Usuario] ( [Email], [Nombre], [Edad], [Altura], [Activo]) VALUES (N'pefadro@gilimail.com', N'Perru', 35, 1.64, 0)
INSERT INTO [dbo].[Usuario] ( [Email], [Nombre], [Edad], [Altura], [Activo]) VALUES (N'pedrdsafao@gilimail.com', N'Perri', 32, 1.36, 0)
INSERT INTO [dbo].[Usuario] ( [Email], [Nombre], [Edad], [Altura], [Activo]) VALUES (N'pefsadfadro@gilimail.com', N'Perre', 31, 1.46, 0)
INSERT INTO [dbo].[Usuario] ( [Email], [Nombre], [Edad], [Altura], [Activo]) VALUES (N'pfdsafaedro@gilimail.com', N'Perra', 33, 1.77, 0)
INSERT INTO [dbo].[Usuario] ( [Email], [Nombre], [Edad], [Altura], [Activo]) VALUES (N'pedfsdafaro@gilimail.com', N'Perro', 23, 1.76, 0)
INSERT INTO [dbo].[Usuario] ( [Email], [Nombre], [Edad], [Altura], [Activo]) VALUES (N'pedfdsafro@gilimail.com', N'Pedri', 53, 1.26, 0)
INSERT INTO [dbo].[Usuario] ( [Email], [Nombre], [Edad], [Altura], [Activo]) VALUES (N'pedsfadro@gilimail.com', N'Pedru', 23, 1.56, 0)
INSERT INTO [dbo].[Usuario] ( [Email], [Nombre], [Edad], [Altura], [Activo]) VALUES (N'pedsafsdro@gilimail.com', N'Pedre', 31, 1.16, 0)
INSERT INTO [dbo].[Usuario] ( [Email], [Nombre], [Edad], [Altura], [Activo]) VALUES (N'pefasdfadro@gilimail.com', N'Pedra', 43, 1.26, 0)
SET IDENTITY_INSERT [dbo].[Usuario] OFF
