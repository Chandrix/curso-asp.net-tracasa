﻿DECLARE @aux_id INT

-- SET @aux_id = U.Id FROM Usuarios

DECLARE cur_u CURSOR FOR
	SELECT u.id from Usuario AS U
OPEN cur_u
FETCH NEXT FROM cur_u INTO @aux_id

WHILE @@FETCH_STATUS = 0
BEGIN
	SELECT SUM(C.cantidad), (SELECT Nombre FROM Usuario WHERE Usuario.Id = @aux_id)
		FROM CompraUsuarioProducto AS C WHERE C.IdUsuario = @aux_id

		FETCH NEXT FROM cur_u
END
CLOSE cur_u
DEALLOCATE cur_u
