﻿-- Para evitar duplicados
-- select distinct Edad from [dbo].Usuario
-- select count(Edad) from [dbo].Usuario

-- select * from [dbo].Usuario where edad > 40
-- select * from [dbo].Usuario where Activo = 'false'
-- select * from [dbo].Usuario where Activo = 0

-- select * from [dbo].Usuario where Activo = 1 and Altura < 2
-- select * from [dbo].Usuario where Activo = 0 or Edad >= 33
-- select * from [dbo].Usuario where not(Activo = 0 or Edad >= 33)
-- select * from [dbo].Usuario order by edad desc, Activo desc
-- select * from [dbo].Usuario where coche is not null

-- Devolver los 5 usuarios de mayor edad sin coche, ordenados alfabeticamente por nombre.

-- select top 5 * from (select top 5 * from [dbo].Usuario where coche is null order by edad desc) as subconsulta order by nombre

-- Devolver cuantos usuarios hay activos y cuantos inactivos
select activo, count(Nombre) from Usuario group by Activo

-- Devuelve los usuarios de menor altura ordenados por edad
select * from (select top 3 * from Usuario order by Altura) as subconsulta order by edad desc

-- Devuelve los usuarios con el mismo coche
select Coche, count(coche) as Numerocoches from (select Coche from usuario where Coche = Coche) as subconsulta group by coche

-- Devolver el usuario de más edad con coche
select top 1 * from usuario where coche is not null order by Edad desc

-- Devolver el usuario de menos edad inactivo y sin coche

select top 1 * from Usuario where coche is null and Activo = 'False' order by edad

-- Calcula la media de edades de los usuarios activos, inactivos, con coche y sin coche

select avg(Edad) as MediaActivos from usuario where Activo = 'true'
select avg(Edad) as MediaInactivos from usuario where Activo = 'false'
select avg(Edad) as MediaConcoche from usuario where coche is not null
select avg(Edad) as MediaSinCoche from usuario where Coche is null
