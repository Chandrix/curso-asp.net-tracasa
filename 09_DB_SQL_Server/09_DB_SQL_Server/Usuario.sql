﻿CREATE TABLE [dbo].[Usuario]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Nombre] VARCHAR(50) NOT NULL, 
    [Edad] TINYINT NOT NULL, 
    [Altura] FLOAT NOT NULL, 
    [Activo] BIT NOT NULL
)
