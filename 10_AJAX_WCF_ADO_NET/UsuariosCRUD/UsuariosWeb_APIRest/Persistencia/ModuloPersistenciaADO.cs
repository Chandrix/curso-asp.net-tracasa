﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace ModeloUsuarios
{
    public enum Entorno
    {
        Ninguno = 0,
        Produccion = 1,
        Desarrollo = 2,
        Preproduccion = 3,
    }
    public class ModuloPersistenciaADO
    {
        string CONEX_BD;
        static Entorno entorno = Entorno.Desarrollo;
        // ModeloUsuario modelo;

        List<Usuario> listaUsuarios = new List<Usuario>();

        public ModuloPersistenciaADO(Entorno entorno)
        {
            ModuloPersistenciaADO.entorno = entorno;

            if (entorno == Entorno.Produccion)
            {
                CONEX_BD = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\DB\\bd_usu.mdf;Integrated Security=True;Connect Timeout=30";
            }
            else
            {
                Console.WriteLine("base de datos de usu test");
                CONEX_BD = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\DB\\bd_usu_test.mdf;Integrated Security=True;Connect Timeout=30";
            }

            ModeloUsuario.Instancia.delegadoCrear = Crear;
            ModeloUsuario.Instancia.delegadoEliminar = Eliminar;
            ModeloUsuario.Instancia.leerTodo = LeerTodo;
            ModeloUsuario.Instancia.leerUno = LeerUno;
            //ModeloUsuario.Instancia.leerConfirmar = LeerConfirmar;
            ModeloUsuario.Instancia.modificar = Modificar;
            ModeloUsuario.Instancia.buscar = BuscarId;
            //listaUsuarios = Leer();

        }
        public int Crear(Usuario usuario)
        {
            bool existe = false;
            foreach (Usuario usu in LeerTodo())
            {
                if (usu.Nombre == usuario.Nombre)
                {
                    existe = true;
                    break;
                }
                else
                {
                    existe = false;

                }
            }
            if (!existe)
            {

                SqlConnection conexion;
                using (conexion = new SqlConnection(CONEX_BD))
                {

                    conexion.Open();
                    SqlCommand comando = conexion.CreateCommand();

                    string insert = $"INSERT INTO Usuario (email, nombre, altura, edad,activo) " +
                        $"VALUES ('{usuario.Nombre.ToLower().Replace(' ', '_')}@gmail.es' , '{usuario.Nombre}', {usuario.Altura.ToString().Replace(',', '.')}, {usuario.Edad},0)";
                    comando.CommandText = insert;

                    comando.ExecuteNonQuery();

                }

                return BuscarId($"{usuario.Nombre}");
            }
            else { return -1; }

        }

        public int BuscarId(string email)
        {
            try
            {
                SqlConnection conexion;

                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();

                    SqlCommand comando = conexion.CreateCommand();
                    comando.CommandText = $"SELECT Id FROM Usuario WHERE email = '{email}@gmail.es'";
                    SqlDataReader lectorDR = comando.ExecuteReader();
                    int id;
                    if (lectorDR.Read())
                    {
                        id = lectorDR.GetInt32(0);
                    }
                    else
                    {
                        id = -1;
                    }
                    conexion.Close();
                    return id;
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error BuscarId bbdd: " + ex.Message);
                return -1;
            }

        }

        public bool Eliminar(int entero)
        {

            try
            {
                SqlConnection conexion;

                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();

                    SqlCommand comando = conexion.CreateCommand();
                    comando.CommandText = "DELETE FROM Usuario WHERE Id = " + entero;
                    int filasAfectadas = comando.ExecuteNonQuery();

                    //desechar
                    if (filasAfectadas != 1)
                        throw new Exception("No se ha eliminado al usuario con ID: " + listaUsuarios[entero].Id
                            + comando.CommandText);

                    return true;
                }   // conexion.Close();
            }
            catch (Exception ex)
            {

                Console.Error.WriteLine("Error EliminarDeBD bbdd: " + ex.Message);
            }
            return false;

        }

        public IList<Usuario> LeerTodo()
        {
            listaUsuarios = new List<Usuario>();

            try
            {
                SqlConnection conexion;

                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();

                    Usuario usuario;
                    SqlCommand comando = conexion.CreateCommand();
                    comando.CommandText = "SELECT Id,nombre, email,altura, edad, activo FROM Usuario";
                    SqlDataReader lectorDR = comando.ExecuteReader();

                    while (lectorDR.Read())
                    {
                        usuario = new Usuario();
                        usuario.Nombre = lectorDR["nombre"].ToString();
                        usuario.Altura = (float)(double)lectorDR["altura"];
                        usuario.Edad = int.Parse(lectorDR["edad"].ToString());
                        usuario.Id = (int)lectorDR["Id"];
                        usuario.Actividad = (bool)lectorDR["Activo"];
                        usuario.Email = lectorDR["email"].ToString();

                        listaUsuarios.Add(usuario);
                    }
                    Console.WriteLine("Leidos de bbdd (LeerTodo):" + listaUsuarios.Count);
                }   // conexion.Close();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Errooooor leyendo bbdd (LeerTodo): " + ex.Message);
            }
            return listaUsuarios;


        }

        public Usuario LeerUno(int entero)
        {
            try
            {
                SqlConnection conexion;

                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();
                    SqlCommand comando = conexion.CreateCommand();
                    comando.CommandText = $"SELECT * FROM Usuario where id='{entero}'";

                    SqlDataReader lectorDR = comando.ExecuteReader();
                    Usuario usuario = new Usuario();
                    while (lectorDR.Read())
                    {
                        usuario = new Usuario();
                        usuario.Nombre = lectorDR["Nombre"].ToString();
                        usuario.Altura = (float)(double)lectorDR["Altura"];
                        usuario.Edad = (byte)lectorDR["Edad"];
                        usuario.Id = (int)lectorDR["Id"];

                    }
                    conexion.Close();
                    return usuario;

                }

            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error leyendo bbdd: " + ex.Message);
                return null;
            }
        }

        public bool Modificar(Usuario usu)
        {
            try
            {
                SqlConnection conexion;

                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();

                    SqlCommand comando = conexion.CreateCommand();

                    comando.CommandText = $"SELECT * FROM Usuario where id='{usu.Id}'";
                    SqlDataReader lecturaOK = comando.ExecuteReader();
                    if (lecturaOK.Read())
                    {
                        lecturaOK.Close();
                        comando.CommandText = $"UPDATE Usuario  SET nombre = '{usu.Nombre}', email = '{usu.Nombre.ToLower().Replace(' ', '_') + "@gmail.es"}',edad = '{usu.Edad}'," +
                            $"altura = {usu.Altura.ToString().Replace(',', '.')} WHERE Id = '{usu.Id}'";

                        //Console.WriteLine(comando.CommandText);
                        int filasAfectadas = comando.ExecuteNonQuery();

                        //desechar
                        if (filasAfectadas != 1)
                            throw new Exception("No se ha modificado al usuario con ID: " + usu.Id
                                + comando.CommandText);
                        //Console.WriteLine("Modificado de bbdd de id:" + usu.Id);
                    }

                }

                conexion.Close();
                return true;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error leyendo bbdd: " + ex.Message);
                return false;
            }

        }

    }
}
