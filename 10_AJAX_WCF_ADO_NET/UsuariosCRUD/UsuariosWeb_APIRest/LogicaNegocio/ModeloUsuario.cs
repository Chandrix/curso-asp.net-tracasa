﻿using System.Collections.Generic;

namespace ModeloUsuarios
{
    public delegate int CrearUsuario(Usuario usuario);
    public delegate bool EliminarUsuario(int entero);
    public delegate IList<Usuario> LeerTodo();
    public delegate Usuario LeerUno(int entero);
    public delegate bool LeerConfimar(string nombre);
    public delegate bool Modificar(Usuario usu);
    public delegate int Buscar(string usu);

    public class ModeloUsuario : IModeloUsuario
    {
        static ModeloUsuario instance = null;
        public CrearUsuario delegadoCrear;
        public EliminarUsuario delegadoEliminar;
        //public LeerUsuario delegateLeer;

        public LeerTodo leerTodo;
        public LeerUno leerUno;
        public LeerConfimar leerConfirmar;
        public Modificar modificar;
        public Buscar buscar;

        public static ModeloUsuario Instancia
        {
            get
            {
                if (instance == null)
                {
                    instance = new ModeloUsuario();
                }

                return instance;
            }
        }

        public int Crear(Usuario nuevoObj)
        {
            if (nuevoObj != null)
            {
                if (!string.IsNullOrEmpty(nuevoObj.Nombre) && nuevoObj.Nombre != "SIN NOMBRE")
                {

                    return (int)(delegadoCrear?.Invoke(nuevoObj));
                }
            }
            return -1;
        }

        public bool Eliminar(int entero)
        {
            return delegadoEliminar.Invoke(entero);
        }

        public IList<Usuario> LeerTodos()
        {
            return leerTodo?.Invoke();
        }

        public Usuario LeerUno(int entero)
        {

            return leerUno?.Invoke(entero);
        }

        public bool LeerConfirmar(string nombre)
        {

            return leerConfirmar.Invoke(nombre);
        }

        public int Buscar(string nombre)
        {

            return buscar.Invoke(nombre);
        }

        public bool Modificar(Usuario usuario)
        {
            return modificar.Invoke(usuario);
        }
    }
}
