﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace ModeloUsuarios
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsuariosController : ControllerBase
    {
        private readonly ILogger<UsuariosController> _logger;
        public UsuariosController(ILogger<UsuariosController> logger)
        {
            _logger = logger;
            modeloUsuario = ModeloUsuario.Instancia;
        }

        IModeloGenerico<Usuario> modeloUsuario;

        [HttpPost("Crear")]
        public int Crear(Usuario nuevoObj)
        {
            return modeloUsuario.Crear(nuevoObj);
        }

        [HttpGet("LeerTodos")]
        public IList<Usuario> LeerTodos()
        {
            return modeloUsuario.LeerTodos();
        }
        [HttpDelete("Eliminar/{entero}")]
        public bool Eliminar(int entero)
        {
            return modeloUsuario.Eliminar(entero);
        }
        [HttpGet("leerUno/{entero}")]
        public Usuario LeerUno(int entero)
        {
            return modeloUsuario.LeerUno(entero);
        }

        [HttpPut("modificar")]
        public bool Modificar(Usuario usuario)
        {
            return modeloUsuario.Modificar(usuario);
        }

        [HttpGet("buscandoId/{email}")]
        public int Buscar(string email)
        {
            return modeloUsuario.Buscar(email);
        }
    }
}
