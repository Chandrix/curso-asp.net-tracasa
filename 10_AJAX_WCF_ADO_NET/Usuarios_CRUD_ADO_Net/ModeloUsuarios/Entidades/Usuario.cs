﻿using System;

namespace ModeloUsuarios
{
    public class Usuario
    {
        protected int id;
        protected string nombre;
        protected int edad;
        protected float altura;
        protected string email;
        protected bool actividad;


        public Usuario(string nombre, int edad, float altura)
        {

            Nombre = nombre;
            Edad = edad;
            Altura = altura;
        }

        public Usuario(int id, string nombre, string email, int edad, float altura, bool actividad)
        {
            Id = id;
            Nombre = nombre;
            Edad = edad;
            Altura = altura;
            Email = email;
            Actividad = actividad;
        }

        public Usuario()
        {

        }

        public override string ToString()
        {
            return "Nombre: " + nombre + " Edad: " + edad + " Altura: " + altura;
        }

        public string GetNombre()
        {
            return nombre;
        }

        public void SetNombre(string nombre)
        {
            this.nombre = nombre;
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    nombre = value;
                }
                else
                {
                    nombre = "SIN NOMBRE";
                }
            }
        }

        public bool Actividad
        {
            get
            {
                return actividad;
            }
            set
            {
                if (value)
                {
                    actividad = value;
                }
                else
                {
                    actividad = false;
                }
            }
        }

        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    email = value;
                }
                else
                {
                    email = "SIN EMAIL";
                }
            }
        }


        public int Edad
        {
            get
            {
                return edad;
            }
            set
            {
                if (value > 0 && value < 120)
                {
                    edad = value;
                }
                else
                {
                    edad = 1;
                }
            }
        }
        public float Altura
        {
            get
            {
                return altura;
            }
            set
            {
                if (value > 0.1f && value < 3f)
                {
                    altura = value;
                }
                else
                {
                    altura = 1f;
                }
            }
        }

        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                if (value >= 0)
                {
                    id = value;
                }
                else
                {
                    throw new Exception("id de usuario invalido(menor que cero)");
                }
            }
        }
        public override bool Equals(object usuario)
        {
            Usuario u = (Usuario)usuario;
            if (base.Equals(usuario))
                return true;
            else
            {
                return (this.Nombre.Equals(u.Nombre)
                    && this.Edad.Equals(u.Edad)
                    && this.Altura.Equals(u.Altura));
            }
        }
    }
}
