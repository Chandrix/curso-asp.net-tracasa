using NUnit.Framework;
using ModeloUsuarios;
using System;
using System.Reflection;
using System.Collections.Generic;

namespace TestModeloUsuario
{
    public class TestsControladorYModelosADO
    {
        ModeloUsuario modelo;
        ModeloUsuario cont_modelo;
        ModuloPersistenciaADO moduloPersistencia;
        private static List<int> Ids_para_eliminar = new List<int>();

        [SetUp]
        public void Setup()
        {
            moduloPersistencia = new ModuloPersistenciaADO(Entorno.Desarrollo);
            modelo = ModeloUsuario.Instancia;
            cont_modelo = modelo;
        }

        [TearDown]
        public void acabandoTest()
        {
            if (Ids_para_eliminar != null)
            {
                foreach (var item in Ids_para_eliminar)
                {
                    cont_modelo.Eliminar(item);
                    Ids_para_eliminar = new List<int>();
                }
            }

        }

        [Test]
        public void TestCrearValidos()
        {

            int id1 = cont_modelo.Crear(new Usuario("testing1", -3, 2.2f));
            int id2 = cont_modelo.Crear(new Usuario("testing2", 99, 1.3f));
            Ids_para_eliminar.Add(id1);
            Ids_para_eliminar.Add(id2);
            Assert.Multiple(() =>
            {
                Assert.AreEqual("testing1", cont_modelo.leerUno(id1).Nombre);
                Assert.AreEqual(99, cont_modelo.leerUno(id2).Edad);
                //etc...

            });

            /* Assert.AreEqual(1, controlador.LeerUno(3).Edad);
              Assert.AreEqual(1.0f, controlador.LeerUno(4).Altura);
              Assert.AreEqual("hola", controlador.LeerUno(0).Nombre);
              Assert.Null(controlador.LeerUno(6));
              Assert.AreEqual(controlador.LeerTodos().Count, 5, "Mal creados los usuarios");
              Assert.IsTrue(controlador.LeerUno(0).Equals(new Usuario(100, "hola", 27, 1.5f)));
            */
        }

        [Test]
        public void TestCrearInvalidos()
        {
            int count = cont_modelo.LeerTodos().Count;
            int id1 = cont_modelo.Crear(new Usuario("testing1", 400, 2.2f));

            //y si id1 es null? falta casuistica
            Ids_para_eliminar.Add(id1);

            Assert.Multiple(() =>
            {
                Assert.AreEqual(1, cont_modelo.leerUno(id1).Edad);
                Assert.AreEqual(cont_modelo.LeerTodos().Count, count + 1, "Se ha introducido un dato erroneo y se ha modificado");
                //etc...

            });
            /*
            cont_modelo.Crear(new Usuario(null, 400, 27));
            cont_modelo.Crear(null);
            Assert.AreEqual(cont_modelo.LeerTodos().Count, 8, "Mal creados los usuarios");*/
        }

        [Test]
        public void TestModificar()
        {

            Usuario usu1mod = new Usuario("testingmodificar", -3, 7);
            Usuario usu1mod2 = new Usuario("testingmodificar2", -3, 7);

            usu1mod.Id = cont_modelo.Crear(usu1mod);
            usu1mod2.Id = usu1mod.Id;

            Ids_para_eliminar.Add(usu1mod.Id);

            Assert.Multiple(() =>
            {
                Assert.AreEqual(true, cont_modelo.Modificar(usu1mod));
                Assert.AreEqual(true, cont_modelo.Modificar(usu1mod2));
                Assert.AreEqual("testingmodificar2", cont_modelo.leerUno(usu1mod2.Id).Nombre);
                //etc...

            });
            /*
            Assert.AreEqual(cont_modelo.LeerUno(0), usu1);
            Assert.AreEqual(cont_modelo.LeerUno(0), usu1mod);
            Usuario usuNoExiste = new Usuario("NoExisto", 88, 88);
            cont_modelo.Modificar(usuNoExiste);
            for (int i = 0; i < cont_modelo.LeerTodos().Count; i++)
            {
                Assert.AreNotEqual(cont_modelo.LeerUno(i), usuNoExiste);
            }*/
        }


        [Test]
        public void TestEliminar() //hecho
        {
            //  Usuario usu1 = new Usuario("hola", 1, 1);
            // controlador.Crear(usu1);

            //ATENCION hecho de dos maneras diferentes, el metodo crear devuelve el id CREADO, el metodo buscar tambien.
            //Assert.AreEqual(true, cont_modelo.Eliminar(cont_modelo.Buscar("testingeliminar")));
            int id1 = cont_modelo.Crear(new Usuario("testingeliminar2", 43, 2));
            int id2 = cont_modelo.Crear(new Usuario("testingeliminar3", 43, 2));

            Assert.Multiple(() =>
            {
                Assert.AreEqual(true, cont_modelo.Eliminar(cont_modelo.Crear(new Usuario("testingeliminar", 43, 2))));
                Assert.AreEqual(true, cont_modelo.Eliminar(cont_modelo.Buscar("testingeliminar2")));
                Assert.AreEqual(true, cont_modelo.Eliminar(cont_modelo.Buscar("testingeliminar3")));
                //etc...

            });
            //  controlador.Crear(new Usuario("qtal", 40, 2));
            //  Usuario usu = controlador.LeerTodos()[1];
            //  Assert.AreEqual(true, controlador.Eliminar(1));
            // foreach (Usuario usuario in controlador.LeerTodos())
            {
                //   Assert.AreEqual(false, usuario == usu);
            }

        }

        [Test]
        public void TestLeerUno()
        {
            int id = cont_modelo.Crear(new Usuario("testingLeerUno", 43, 2));
            Ids_para_eliminar.Add(id);

            Assert.Multiple(() =>
            {
                Assert.AreNotEqual(null, cont_modelo.LeerUno(id));
                Assert.AreEqual("testingLeerUno", cont_modelo.LeerUno(id).Nombre);
                //etc...

            });

            //cont_modelo.Eliminar(cont_modelo.Buscar("testingLeerUno"));
            //  Usuario usu1 = new Usuario("testing1", 1, 1);
            // controlador.Crear(usu1);
            // controlador.Crear(new Usuario("adios", 43, 2));
            //  controlador.Crear(new Usuario("qtal", 40, 2));
            // Assert.Null(controlador.LeerUno(4));
            //Assert.NotNull(controlador.LeerUno(1));
            //Assert.IsInstanceOf<Usuario>(controlador.LeerUno(1));

        }

        [Test]
        public void TestLeerTodos()
        {
            int count = cont_modelo.LeerTodos().Count;
            int id1 = cont_modelo.Crear(new Usuario("testingLeerTodos1", 43, 2));
            int id2 = cont_modelo.Crear(new Usuario("testingLeerTodos2", 40, 2));
            Ids_para_eliminar.Add(id1);
            Ids_para_eliminar.Add(id2);

            Assert.Multiple(() =>
            {
                Assert.AreEqual(count + 2, cont_modelo.LeerTodos().Count);
                //etc...

            });

        }

    }

}
